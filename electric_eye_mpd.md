\*\* Meeting \*MONDAY\* 29th June \*\*

| Topic                                 | Presenter      | Slides                                                        |
| ------------------------------------- | -------------- | ------------------------------------------------------------- |
| Electric Eye - Network Video Recorder | Michael Pope   | [electric\_eye slides](/workshops/20150629-electric_eye.pdf) |
| MPD - Music Player Daemon             | James Pinkster | [mpd slides](/workshops/20150629-mlug-music_server.pdf)      |

Please show your interest on our mailing list or email me directly
