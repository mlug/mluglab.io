## Meeting; 26th October 2015

### Malcolm: rcorder

rcorder looks at the order of start up processes for BSD

reorder files to access those files.

kick off files in the right order. Looks at all the dependencies and
orders all the startup files correctly.

Can use rcorder to create a DB schema in order of dependencies. So,
using a tool which is for startup for creating a database in order.

### Malcolm: PostgreSQL $stuff

\*You can use Postgresql's copy to read tsv files directly into
postgres\*

PostgreSQL methods coalesce will look in two columns and look at the
first for a value if it's null then it will get the next.

### Darren: Docker

AWS - spot network cheap testing VM instances. Could get some real power
for cheap but also you could just get kicked off at any time

<https://aws.amazon.com/free/>

AWS sydney is fast.

Prove that docker is running proper : sudo docker run hello-world

NOTE: 'sudo \!\!' will run the last command as sudo

gitbucket is an open source github thing.

gitlab - ruby but requires a lot of stuff to do the same thing.

network presence VPS is very cheap $3 p/mth.

<https://gist.github.com/dwurf/c9bb3f46209d444bc69b>
