# Openstreetmap

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 27th April, 2011                                          |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Cameron                                                             |
| **Workshop Topic** | Openstreet Map in all it's glory                                    |

[Presentation Slides](/workshops/openstreetmap.pdf)

### URL's mentioned

OpenStreetBugs: http:*openstreetbugs.schokokeks.org/  
Cloudmade maps: http:*maps.cloudmade.com/  
Flickr: <http://www.flickr.com/map/> - Uses osm for high zoom in Sydney
(though very out of date)  
Map compare: http:*tools.geofabrik.de/mc/  
MapOSMatic http:*www.maposmatic.org  
Mapquest: http:*open.mapquest.co.uk/link/h/2-qTGJsY0V  
Marble: http:*edu.kde.org/marble/  
Nominatim: http:*wiki.openstreetmap.org/wiki/Nominatim  
OpenCuisineMap: http:*toolserver.org/\~stephankn/cuisine/  
OpenOrienteeringMap: http:*oobrien.com/oom/  
OpenStreetMap: http:*www.openstreetmap.org/  
OpenStreetMap routing demo: http:*osmu.org/demo/index.html  
Transparent map comparison: http:*sautter.com/map/  

### Notes taken at the meeting of interest

There were a lot of questions asked at this meeting.

JOSM Java OpenStreetMap Editor (Best editor for detailed changes)  
PostGIS Postgress with GIS is what OSM uses under the hood  
www.osm.org licencing going thru changes presently  
ANDNAV2 for Android App  
Routing is not available through the OSM website but is handled by
devices such as Garmin which you can load OSM onto.  
Searches using Nominatim  
NAVIT used in conjunction with downloaded OSM database, installed onto
TOM TOM.  
Ability to frequently download DELTA changes to OSM database to update
personal copies of maps.
