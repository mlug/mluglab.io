# LUKS and IPv6

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | 2nd May, 2012                                                       |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Tim Rice, Darren Wurf                                               |
| **Workshop Topic** | Encrypting with LUKS, IPv6                                          |

## LUKS

![LUKS scripts](/workshops/cryptscripts2.tar.gz)

## IPv6

![IPv6 Slides](/workshops/20120502-ipv6.odp)
