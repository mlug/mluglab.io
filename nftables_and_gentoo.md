**26th February 2018**

| Talk     | Speaker      | Slides                                    |
| -------- | ------------ | ----------------------------------------- |
| nftables | Duncan Roe   |                                           |
| Gentoo   | Danny Robson | [slides](/workshops/20180226-gentoo.pdf) |
