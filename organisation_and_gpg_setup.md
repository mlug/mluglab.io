# Organising yourself and GPG Setup

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | 28th March, 2012                                                    |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Bianca Gibson, David Schoen                                         |
| **Workshop Topic** | Organising yourself, GPG Setup                                      |

![Organising yourself slides](/workshops/organisation_slides.odp)  
[GPG slides](/workshops/mlug-gnuprivacyguard.pdf)
