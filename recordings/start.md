# Recordings

Here are some voice recordings of past meetings.

Note: The quality levels may fluctuate.

## August 2012

[Data
Mining](http://www.mlug.org.au/recordings/MLUG_2012-08-30_Lauchlin_Wilkinson-Data_Mining.ogg)

## May 2012

[Arduino](http://www.mlug.org.au/recordings/mlug_2012_05_02_arduino.ogg)
[C pdf tools](http://www.mlug.org.au/recordings/mlug_2012_05_02_c.ogg)

## April 2012

[IPv6](http://www.mlug.org.au/recordings/mlug_2012_05_02_ipv6.ogg)
[LUKS](http://www.mlug.org.au/recordings/mlug_2012_05_02_luks.ogg)

## March 2012

[GPG
part1](http://www.mlug.org.au/recordings/MLUG_2012-03-28_GPG-part1.ogg)
[GPG
part2](http://www.mlug.org.au/recordings/MLUG_2012-03-28_GPG-part2.ogg)
[Open Source Organisation
Tools](http://www.mlug.org.au/recordings/MLUG_2012-03-28_Open_Source_Organisation_Tools.ogg)
