\*\*Monday 28th November, 2016 \*\*

| Topic                                 | Speaker      | Slides                                  |
| ------------------------------------- | ------------ | --------------------------------------- |
| Cleaning up large data files with CLI | Bob Mesibov  | <http://www.polydesmida.info/cookbook/> |
| Building a mechanical keyboard        | Danny Robson | [](/workshops/aukb.pdf)                |
