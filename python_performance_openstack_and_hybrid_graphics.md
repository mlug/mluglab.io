# Python performance, Openstack and Hybrid graphics

Wednesday 23rd January, 2013 @ 7pm

## Coding performance, why C is not faster than Python

by Darren Wurf

[Slides](http://prezi.com/uxngm--lagzb/code-performance-why-c-is-not-faster-than-python/)

Downloadable (120MB)

``` 
  git clone git://github.com/dwurf/mlug-c-python-prezi.git
```

Code: <https://github.com/dwurf/mlug-c-python>

## OpenStack, open source cloud computing software

\- Mark Atwood

[Slides](http://fallenpegasus.github.com/slidy-intro-openstack-melbourne)

## Getting hybrid graphics working

\- Michael Pope

![Slides](/workshops/20130123-mlug.odp)
