\*\*30th (Thursday) March, 2017 \*\*

| Topic                                  | Speaker    | Slides                                          |
| -------------------------------------- | ---------- | ----------------------------------------------- |
| synclink with csv2anki (short)         | Bob Parker |                                                 |
| Slackware package manager for everyone | Duncan Roe | ![slides](/workshops/20170330-slackinstall.odp) |
