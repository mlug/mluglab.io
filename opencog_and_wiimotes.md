# OpenCOG & WIIMOTES

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 26th October, 2011                                        |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Cameron & Rich Healey                                               |
| **Workshop Topic** | Control your linux computer with WIIMOTES & OpenCOG                 |

## OpenCOG

You can view my slides on prezi here:
<http://prezi.com/60ezwxkx3ixp/opencog-mlug-october/>

Also, for those to whom I mentioned the mostly wrong history of
programming, this might get you a giggle

<http://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html>

Finally, Michael you might be interested to check out pentadactyl for
firefox:

<http://dactyl.sourceforge.net/pentadactyl/>

vimpc - mpd client

<https://github.com/richoH/vimpc>

For those of you on Debian sid or testing you can use the psych0tik repo
which has current builds for amd64 and currrentish builds for i386

    deb http://packages.psych0tik.net/apt/ sid main

## WIIMOTE

![WIIMOTE slides](/workshops/wiimote_mlug_presentation.odp)
