| Topic       | Speaker      | Slides                                                 |
| ----------- | ------------ | ------------------------------------------------------ |
| Datascience | Darren       |                                                        |
| Syncthing   | Michael Pope | [syncthing\_slide](/workshops/20160530-syncthing.pdf) |
