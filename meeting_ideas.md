# Meeting Ideas

Here are some ideas which maybe of interest for future meetings. If you
are interested in doing a workshop about one of these topics or
something else related to Linux please let us know on the mailing list.

System Admin

  - Using ZSH
  - BTRFS filesystem tips and tricks
  - Creating a software raid
  - Virtualisation with Xen/KVM installation and configuration
  - nagios -\> performance network monitoring tool
  - Setting up Asterisk/Trixbox
  - hylafax -\> semi-interest.
  - Setting up a cloud storage server using
    [iFolder](https://help.ubuntu.com/community/ifolderinstall)
  - Using A proxy to keep a central repository of DEB packages you
    install to save bandwidth and speed up installation
  - Streaming music using mp-daap and amarok over wireless

Programming

  - Testing your administration scripts with cucumber / rspec.
  - Add a GUI to your scripts with Shoes
  - Configuring emacs for Rails|
  - Starting to program Python with QT
  - Version control systems 

Hacking

  - BYO Router and install
    [OpenWRT](http://openwrt.org/)/[Tomato](http://www.polarcloud.com/tomato)/etc.
    on it.
  - BYO a box which either runs linux or has the potential to run linux
    and we can hack it. EG: 
      - [Dlink DNS-323 NAS](http://wiki.dns323.info/) 
      - [Linksys NSLU2](http://www.nslu2-linux.org/) aka SLUG.
      - [TomTom](http://www.opentom.org/main_page)
