# Virtual Hosts

Host: Robert Moonen Date: 29th May, 2009

## Setting up Virtual Hosts under Apache.

Make sure that the module you need is available.

So first of all we need to check the directory of available modules to
see if we have a copy of module vhosts\_alias

    ls /etc/apache2/mods-available/

    actions.conf
    actions.load
    alias.conf
    alias.load
    asis.load
    auth_basic.load
    auth_digest.load
    authn_alias.load
    authn_anon.load
    authn_dbd.load
    authn_dbm.load
    authn_default.load
    authn_file.load
    authnz_ldap.load
    authz_dbm.load
    authz_default.load
    authz_groupfile.load
    authz_host.load
    authz_owner.load
    authz_user.load
    autoindex.conf
    autoindex.load
    cache.load
    cern_meta.load
    cgi.load
    cgid.conf
    cgid.load
    charset_lite.load
    dav.load
    dav_fs.conf
    dav_fs.load
    dav_lock.load
    dbd.load
    deflate.conf
    deflate.load
    dir.conf
    dir.load
    disk_cache.conf
    disk_cache.load
    dump_io.load
    env.load
    expires.load
    ext_filter.load
    file_cache.load
    filter.load
    headers.load
    ident.load
    imagemap.load
    include.load
    info.conf
    info.load
    ldap.load
    log_forensic.load
    mem_cache.conf
    mem_cache.load
    mime.conf
    mime.load
    mime_magic.conf
    mime_magic.load
    negotiation.conf
    negotiation.load
    perl.load
    php5.conf
    php5.load
    proxy.conf
    proxy.load
    proxy_ajp.load
    proxy_balancer.load
    proxy_connect.load
    proxy_ftp.load
    proxy_http.load
    python.load
    rewrite.load
    setenvif.conf
    setenvif.load
    speling.load
    ssl.conf
    ssl.load
    status.conf
    status.load
    substitute.load
    suexec.load
    unique_id.load
    userdir.conf
    userdir.load
    usertrack.load
    version.load
    vhost_alias.load    # this is mod_vhost_alias

Next we need to tell apache2 to load the vhosts\_alias module

    ls /etc/apache2/mods-enabled/

    alias.conf
    alias.load
    auth_basic.load
    authn_file.load
    authz_default.load
    authz_groupfile.load
    authz_host.load
    authz_user.load
    autoindex.conf
    autoindex.load
    cgi.load
    deflate.conf
    deflate.load
    dir.conf
    dir.load
    env.load
    mime.conf
    mime.load
    negotiation.conf
    negotiation.load
    perl.load
    php5.conf
    php5.load
    python.load
    setenvif.conf
    setenvif.load
    status.conf
    status.load
    vhost_alias.load    # make sure it's enabled

Now we need to setup the enabled sites file to include all the virtual
hosts we will be adding.

    cat /etc/apache2/sites-enabled/000-default

    <VirtualHost *:80>              # set the port number here
        ServerAdmin webmaster@localhost
        
        DocumentRoot /var/www/
        <Directory />
            Options FollowSymLinks
            AllowOverride None
        </Directory>
        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride AuthConfig
            Order allow,deny
            allow from all
        </Directory>
        UseCanonicalName Off            #
        VirtualDocumentRoot /var/www/%0/    # this is the asked for URL
    
        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
            AllowOverride None
            Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
            Order allow,deny
            Allow from all
        </Directory>
    
        ErrorLog /var/log/apache2/error.log
    
        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn
    
        CustomLog /var/log/apache2/access.log combined
    
        Alias /doc/ "/usr/share/doc/"
        <Directory "/usr/share/doc/">
            Options Indexes MultiViews FollowSymLinks
            AllowOverride None
            Order deny,allow
            Deny from all
            Allow from 127.0.0.0/255.0.0.0 ::1/128
        </Directory>
    
    </VirtualHost>

Including VirtualDocumentRoot /var/www/%0/ in the enabled sites file
tells apache2 that is is to do directory expansion adding the asked for
URL to the normal DocumentRoot directory.

    ls -la /var/www
    total 24 
    drwxrwxr-x  5 root   robert 4096 May 29 20:33 . 
    drwxr-xr-x 14 root   root   4096 Jan  3 13:29 .. 
    lrwxrwxrwx  1 root   root     25 Jan 19 19:03 172.16.0.104 -> lampp/djelectro/postcode/                 # local Virtual host
    lrwxrwxrwx  1 root   root     22 Jan 19 19:02 ares -> ares.going-places.org/                        # local Virtual host
    lrwxrwxrwx  1 robert robert    8 May 26 14:09 ares-ext -> external                      # local Virtual host
    drwxr-xr-x 25 robert robert 4096 May 31 14:45 ares.going-places.org                         # local Virtual host
    drwxr-xr-x  5 robert robert 4096 May 31 14:47 external                              # directory for external access
    lrwxrwxrwx  1 robert robert   18 Jan 24 23:29 going-places.dyndns.org -> /var/www/external/             # dyndns Virtual host
    lrwxrwxrwx  1 robert robert   34 Jan 24 23:29 going-places.webhop.net -> /var/www/lampp/djelectro/postcode/ # dyndns Virtual host
    -rw-r--r--  1 robert robert   45 Dec 10 00:17 index.html 
    drwxr-xr-x  7 robert robert 4096 Jan  8 18:39 lampp 
    lrwxrwxrwx  1 robert robert   17 May 25 21:02 mywebspace.dyndns.org -> /var/www/external            # dyndns Virtual host
    lrwxrwxrwx  1 robert robert   17 May 26 13:54 pastebin.dyndns.org -> /var/www/external              # dyndns Virtual host
    lrwxrwxrwx  1 robert robert   18 Feb  3 12:51 www.going-places.dyndns.org -> /var/www/external/             # dyndns Virtual host
    lrwxrwxrwx  1 robert robert   34 Feb  3 12:48 www.going-places.webhop.net -> /var/www/lampp/djelectro/postcode/     # dyndns Virtual host
    lrwxrwxrwx  1 root   root     16 Jan 19 19:04 www.intra.net -> lampp/djelectro/                     # local Virtual host
    lrwxrwxrwx  1 robert robert   17 May 25 21:02 www.mywebspace.dyndns.org -> /var/www/external            # dyndns Virtual host
    lrwxrwxrwx  1 robert robert   17 May 26 13:54 www.pastebin.dyndns.org -> /var/www/external          # dyndns Virtual host

As you can see, now all you have to do is put links to your directories
in URL format and it all "just works". ;)
