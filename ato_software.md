# ATO software

![CSI software for Linux](/csiinstallfilesforlinux.zip) - A java based
plugin for your Firefox to access the ATO portal for accounting
purposes.

AusKey - Yet to come\!

If you are trying to get AusKey working under Linux and found that it's
unsupported then please file a complaint:  
<https://olt.ato.gov.au/feedback/Complaints_Feedback.asp>

[Etax feedback (Ask for a Linux
version)](http://www.ato.gov.au/feedback/etax.asp)
