# Arch, Smokeping & Wireless Cameras

26th March

|                                      |              |                                                              |
| ------------------------------------ | ------------ | ------------------------------------------------------------ |
| Overview of the Arch distro          | Ben Kaiser   | <http://slid.es/benjkaiser/arch-linux>                       |
| Smokeping                            | James        | ![](/workshops/201402-smokeping.ppt)                         |
| Wireless security system on a budget | Michael Pope | <http://map7.github.io/wireless_camera_security_on_a_budget> |
