# Web Scalability & PostScript

**Wednesday 25th September, 2013 @ 7pm (pre-meeting downstairs at
6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

## Web Scalability by Darren Wurf

<http://dwurf.github.io/web-scalability-presentation>

## PostScript by Malcolm Herbert
