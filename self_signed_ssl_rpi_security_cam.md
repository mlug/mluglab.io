# Self Signed SSL & Raspberry PI Security Cameras

**Meeting Topics for 29th October**

|                                              |         |                                                               |
| -------------------------------------------- | ------- | ------------------------------------------------------------- |
| Self signed SSL certificates                 | James P | <http://jp-it.net.au/presentations/mlug-tls-certificate.html> |
| Raspberry PI inexpensive security IP cameras | Cameron | [motionPIE slides](/workshops/motionpie.pdf)                 |
