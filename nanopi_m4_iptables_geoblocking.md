**29th October 2018**

| Talk                                     | Speaker      | Slides                                       |
| ---------------------------------------- | ------------ | -------------------------------------------- |
| NanoPI M4                                | Michael Pope | [slides](/workshops/20181029-nanopi_m4.pdf) |
| IP validation, IPtables, and GeoBlocking | Timothy Rice |                                              |

## IP Validation Notes

A couple of people asked to have a closer look at Cider, you can find it
at:

<https://notabug.org/cryptarch/cider>

If you're interested in the Hacker News article which opened my eyes to
the craziness of IP address parsing, you can find it here:

<https://news.ycombinator.com/item?id=6580592>

Note that the IPv4 spec acknowledges IP addresses in either octet-dot or
unsigned 32-bit integer form:

<https://tools.ietf.org/html/rfc791#section-2.3>

Thus, the abbreviations from that Hacker News article were not
anticipated and are completely implementation dependent. You can inspect
how implementations can differ by comparing the respective iptables and
ipset interpretations of 127.1/32

For a more recent example illustrating IP addresses in a form that seems
unconventional, these will all take you to a certain popular website:

<http://1000849999> <http://0x3BA7C24F> <http://07351741117>

To get started with ipset, this is the Arch Linux wiki article I
referred to:

<https://wiki.archlinux.org/index.php/Ipset>
