# Blogs & Emacs

27th August

|                                         |               |                                                                        |
| --------------------------------------- | ------------- | ---------------------------------------------------------------------- |
| Moving blogs from blogger to github     | Alan Chalmers | ![slides](/workshops/20140827-moving_blogs_from_blogger_to_github.zip) |
| Using emacs as an offline blogging tool | Michael Pope  | ![slides](/workshops/20140827-travel_blog_tools.zip)                   |
| Control firefox from emacs              | Wenshan       | ![slides](/workshops/20140827-control_firefox_from_emacs.zip)          |
