# Tour de Chateau & LVM

**Wednesday 31st July, 2013 @ 7pm (pre-meeting downstairs at 6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

## Tour de Chateau

Malcolm Herbert presented a talk to do with video editing the Tour de
France to capture all the helicopter views only. He did it by using
filtering on the sound that the Helicopter motors make.

[](/workshops/20130731-talk-chateaux.pdf)

## LVM

I gave a quick live demo of LVM snapshoting which I find useful.

Create snapshot

    lvcreate -L5G -s -n rootsnap /dev/vg.lvm.sol/root

Do something risky ie: an upgrade

    apt-get update; apt-get upgrade; apt-get dist-upgrade

Upgrade went ok? - Yes: Delete snapshot

``` 
  lvremove /dev/vg.lvm.sol/rootsnap
```

\- No: Revert to snapshot & reboot

``` 
  lvconvert --merge /dev/vg.lvm.sol/rootsnap
  reboot
```
