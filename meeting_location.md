# Meeting Location

## Address

|                |                                          |
| -------------- | ---------------------------------------- |
| Place          | **The Great Northern Hotel**             |
| Address        | 644 Rathdowne St, Carlton North VIC 3054 |
| Pre-meeting    | 6pm dinning function room                |
| Time for Talks | 7:30pm in the dinning function room      |
| Website        | <http://gnh.net.au>                      |
| Phone          | (03) 9380 9569                           |

## Getting there

**Public Transport**  
Get a train to Melbourne Central station then it's quicker to walk up to
Lygon St.

**Parking**  
Parking should be available in the area after hours

## Schedule

There is a pre-meeting at 6pm downstairs  
The meeting happens at 7pm upstairs.

On the last Wednesday of every month except December as we have a social
meeting instead.
