## ChromeOS

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 30th March, 2011                                          |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Robert Moonen                                                       |
| **Workshop Topic** | Preview of ChromeOS                                                 |

Robert took us on tour of ChromeOS and we got to look at what's included
and what it's purpose is. It's still not out yet and you have to compile
it to get the distro to work. It mainly looks like a locked down distro
which boots straight into the browser. We did manage to get into the
backend and see that tools such as 'perl' are included in the distro.
