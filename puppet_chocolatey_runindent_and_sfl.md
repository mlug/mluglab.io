**26th September, 2016**

| Topic                             | Speaker         | Slides                     |
| --------------------------------- | --------------- | -------------------------- |
| Puppet and Chocolatey (lightning) | Malcolm Herbert |                            |
| runindent and sfl (lightning)     | Duncan Roe      | ![sfl](/workshops/sfl.odp) |
