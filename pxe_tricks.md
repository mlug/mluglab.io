# PXE Tricks

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 29th June, 2011                                           |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Ed Chan                                                             |
| **Workshop Topic** | PXE booting tricks (thin clients)                                   |

Ed took us through a two part presentation starting with tricks you can
perform with PXE booting involving System Rescue CD. This included how
you can control your computer remotely using a serial connector and the
advantages of doing this and a live demo of creating a client which in
turn becomes a PXE server to supply a PXE boot file to another diskless
client.

The second part of Ed's talk showed us how to install CentOS over PXE
and showed us some of the highlights of using kickstart to automate your
CentOS installs. (FAI for debian is the same but more powerful as it
includes perl and some hook ins to the install procedure).

Thanks Ed for your talk. Ed is on our mailing list and is happy to
answer questions in relation to his talk.

![Presentation notes](/workshops/20110629-pxe_tricks.tar.gz)  
![Config files used](/workshops/20110629-config_files.tar.gz)
