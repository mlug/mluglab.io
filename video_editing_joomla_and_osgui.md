# Video Editing, Joomla and OSGUI setup

|                |                |
| -------------- | -------------- |
| **Date:**      | 27th Oct, 2010 |
| **Hosted By:** | Tim Mullins    |

How Tim has setup his website called OSGUI to stream videos about Linux
and open source to the public. This meeting also detailed discussions
about video editing and what format to use for youtube and such systems
and also the website CMS tool Joomla.

![ko1qY8MlBoc](/youtube\>large/ko1qY8MlBoc)
