**24th April 2017**

| Topic                           | Speaker             | Slides                                         |
| ------------------------------- | ------------------- | ---------------------------------------------- |
| github and collaborative coding | Mike Hewitt (short) | ![slides](/workshops/20170424-git-talk.odp)    |
| Virtualisation for gaming       | Danny Robson        | [slides](/workshops/20170424-gaming_virt.pdf) |
