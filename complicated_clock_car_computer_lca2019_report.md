**28th January 2019**

| Talk                         | Speaker      | Slides                                   |
| ---------------------------- | ------------ | ---------------------------------------- |
| Really complicated car clock | Michael Pope | [slides](/workshops/20190128-carpi.pdf) |
| LCA2019 report               | Duncan Roe   |                                          |
