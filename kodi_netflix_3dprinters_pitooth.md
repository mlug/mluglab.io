**25th February 2019**

| Talk                                              | Speaker      | Slides                                                      |
| ------------------------------------------------- | ------------ | ----------------------------------------------------------- |
| PiTooth - Raspberry Pi as a Bluetooth/USB adapter | Danny Robson | [slides](/workshops/quimby__a_bluetooth-usb_hid_proxy.pdf) |
| Kodi + Netflix, 3dprinters, etc                   | Michael Pope | [slides](/workshops/20190225-kodi_3dprinters_etc.pdf)      |
