# eeepc Hacking

Date: 27th June, 2008  
Hosted By: John Coombes  
This workshop was about modifying the eeepc 701 Xandros Standard system.


<BR>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>eeepc Tips-Notes</B></FONT></P>
<P STYLE="margin-bottom: 0in">NOTE: most of the following has been
taken from Mick Pope's private Google Doc's to which I (johncoom)
have been given access to. These Tips/Notes do not include everything
that is in Mick's Google Doc's, just the most useful stuff with
modifications.</P>
<P STYLE="margin-bottom: 0in">
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="bqmd"></A>Refer to the
eeeuser.com forum postings for any problems I've (Mick) had and
overcome. Most links in here will be to eeeuser.com forum posts or
the wiki.</P>
<P STYLE="margin-bottom: 0.2in"><A HREF="http://www.eeeuser.com/">http://www.eeeuser.com/</A><BR>The
links to Forums and Wiki etc. are on this main page</P>
<P STYLE="margin-bottom: 0.2in">
</P><BR>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>Remove
unionfs</B></FONT></P>
<P STYLE="margin-bottom: 0.2in"><A HREF="http://wiki.eeeuser.com/howto:removeunionfs">http://wiki.eeeuser.com/howto:removeunionfs</A></P>
<P STYLE="margin-bottom: 0.2in"><A NAME="cksm"></A>Note: You must
FIRST install Rescue Mode as per
<A HREF="http://wiki.eeeuser.com/howto:installrescuemode">http://wiki.eeeuser.com/howto:installrescuemode</A></P>
<P STYLE="margin-bottom: 0.2in">Having Unionfs allows you to Restore
Factory Settings because all the original O/S and the applications
are retained in a separate partition on the SDD of the EeePC. Even
when one does an 'apt-get' or uses 'synaptic' to update applications,
the original version of the application is still retained in the
separate partition ! It is rather a waste of space and it is easy to
soon run out of space on the SDD drive. This is why it is often very
useful to remove unionfs and merge the Restore partition with the
first partition of the SDD of the EeePC. (see on line doc's above)</P>
<P STYLE="margin-bottom: 0.2in"><B><BR><FONT SIZE=4>Bios
Updates</FONT><BR></B><BR><A HREF="http://forum.eeeuser.com/viewtopic.php?id=3485">http://forum.eeeuser.com/viewtopic.php?id=3485</A></P>
<P STYLE="margin-bottom: 0.2in">NB: I (johncoom) have not bothered
with the Bios updates my self yet</P><BR>
<P STYLE="margin-bottom: 0.2in">
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="w9:3"></A><B><SPAN STYLE="font-style: normal"><FONT SIZE=4>Restore
Factory Settings</FONT></SPAN></B> 
</P>
<P STYLE="margin-bottom: 0.2in"><FONT SIZE=3>If you are still running
unionfs and you want to restore factory settings you can quickly hit
'F9' as soon as you start your eeepc, then select restore factory
settings from the menu.</FONT></P><BR>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>Tweakeee</B></FONT></P>
<P STYLE="margin-bottom: 0.2in"><A NAME="yz8f"></A><A NAME="he56"></A><A NAME="he560"></A><A NAME="f_fv"></A>
Load up Tweakeee - does most of these settings in this document
through a python-gui app.<BR><BR>$ sudo dpkg -i
tweakeee_0.4-0.i386.deb<BR><BR><A HREF="http://www.infinitedesigns.org/archives/189">http://www.infinitedesigns.org/archives/189</A></P>
<P STYLE="margin-bottom: 0.2in">Make sure that you get the latest
Beta 0.4 of Tweakeee, its linked from the above:</P>
<P STYLE="margin-bottom: 0.2in"><A HREF="http://www.infinitedesigns.org/archives/195">http://www.infinitedesigns.org/archives/195</A></P>
<P STYLE="margin-bottom: 0.2in">
</P>
<P STYLE="margin-bottom: 0.2in">This installs Tweakeee to the
“Settings” tab of your EeePC. Note: That many more 'repositories'
will get added to the /etc/apt/sources.list for you to install extra
applications via the Tweakeee GUI. Some things will not be included
with Tweakee and require that you install via 'apt-get' (or
'synaptic”). But there are quite a lot of things that can be
installed from Tweakeee, plus there is some very useful 'extra
config' items with Tweakeee ! It is well worth the installation.</P>
<P STYLE="margin-bottom: 0.2in"><BR>
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="js4e1"></A><A NAME="disabling_at_startup_only1"></A><A NAME="rj1n1"></A>
<FONT SIZE=4><B>Re-install Xandros 4</B></FONT></P>
<P STYLE="margin-bottom: 0.2in">This can be done using an external
DVD unit plugged in to a USB socket. Shut down the EeePC and with
external DVD plugged in (plus any external power on) then re-start
the EeePC and at the very first AUSE splash screen press the ESC key.
(some times takes several tries). Follow the instructions and it will
wipe your existing system and re-install the original Xandos off the
ASUS DVD that came with your EeePC.</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="qu3w1"></A>This can also be
done using a USB-stick, but first you have to put the Xandros
installer on the stick and is much more complex than using an
external DVD unit which is very easy to do. If you still want to
re-install from a USB-stick ? Then refer to (search) the
<A HREF="http://wiki.eeeuser.com/howto:startupscript">http://forum.eeeuser.com/</A>
site for a HowTo</P>
<P STYLE="margin-bottom: 0.2in"> 
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="si4w21"></A><A NAME="a980"></A><A NAME="si4w2"></A><A NAME="a9800"></A><A NAME="zpxr"></A><A NAME="zpxr0"></A>
<B>Copy bootable linux CD to USB </B><SPAN STYLE="font-weight: medium">(you
could try the following re Mick's Doc's)</SPAN><B><BR></B>$ sudo
apt-get install syslinux mtools<B><BR><BR></B>$ cp -r &lt;cd rom
mount&gt; &lt;usb mount point&gt;<BR>$ sudo syslinux -f &lt;usb
device&gt;</P>
<P STYLE="margin-bottom: 0.2in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>Disabling SD/USB popup at startup only (or completely)</B></FONT></P>
<P STYLE="margin-bottom: 0.2in"><A NAME="xzvi"></A><A HREF="http://wiki.eeeuser.com/howto:tempdisabledevicedetection">http://wiki.eeeuser.com/howto:tempdisabledevicedetection</A></P>
<P STYLE="margin-bottom: 0.2in"><A NAME="disabling_it_always"></A>This
saves you having to bother with the default file manager pop-up each
time and also applies to other plugged in devices. But not a device
plugged in after boot, if you do the 'Disabling it always' option.</P>
<P STYLE="margin-bottom: 0.2in"><BR>
</P>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>Vncserver startup</B></FONT></P>
<P STYLE="margin-bottom: 0.2in"><A NAME="n3_d"></A>Add this to your
.icewm/startup file: 
</P>
<PRE STYLE="margin-bottom: 0.2in"><A NAME="dwka"></A>vnc4server &amp;</PRE><P STYLE="margin-bottom: 0.2in">
<A NAME="r1w:"></A>To use vnc with desktop 0 you have to use x11vnc 
</P>
<PRE STYLE="margin-bottom: 0.2in"><A NAME="uj2y"></A>$ sudo apt-get install x11vnc</PRE><P STYLE="margin-bottom: 0.2in">
<A NAME="tz2s"></A>add a line like this to your startup file: 
</P>
<PRE STYLE="margin-bottom: 0.2in">/usr/bin/x11vnc -noshm -q -passwd alchester -notruecolor -forever
</PRE><BR>
<P STYLE="margin-bottom: 0.2in"><A NAME="rj1n"></A><FONT SIZE=4><B>Extra software worth installing</B></FONT></P>

Some tips from Mick, he wrote:- </SPAN><I>This is a list of packages I've
installed, without a problem just using apt-get, with the
repositories above.</I><SPAN STYLE="font-style: normal">(eg. The
repositories as installed with Tweakeee I presume)</SPAN></FONT></SPAN></H2><BR>
<P STYLE="margin-bottom: 0.2in"><SPAN STYLE="font-style: normal"><FONT SIZE=3><SPAN STYLE="font-weight: medium">gftp
- light weight ftp<BR>pcman - file manager<BR><BR>mrxvt - light
weight terminal<BR>evince - pdf viewer<BR>gkrellm - monitor<BR>abiword
- light weight word processor<BR>gnumeric - light weight
spreadsheet<BR><BR>openssh-server<BR>telnet<BR>rsync - for syncing
files remotely.<BR><BR>xchat<BR><BR>w32codecs<BR><BR>hdparm - hard
drive benchmarks<BR></SPAN><B><BR></B><SPAN STYLE="font-weight: medium">gimp<BR>wine</SPAN></FONT></SPAN></P>
<P STYLE="margin-bottom: 0.2in; font-style: normal; font-weight: medium">
<FONT SIZE=3>Comment by johncoom: you may not want to install all of
the above packages, just do the ones that you want.</FONT></P>
<P STYLE="margin-bottom: 0.2in; font-style: normal; font-weight: medium">
</P>
<P STYLE="margin-bottom: 0.2in; font-style: normal"><FONT SIZE=4><B>Making
MRXVT it default</B></FONT></P>
<P STYLE="margin-bottom: 0.2in; font-style: normal; font-weight: medium">
<FONT SIZE=3>$ sudo update-alternatives --config
x-terminal-emulator<BR>lxterm is the default, select mrxvt from the
list, now when you hit alt+ctrl+t you will get mrxvt.</FONT></P>
<P STYLE="margin-bottom: 0.2in; font-style: normal; font-weight: medium">
<BR><BR>
</P>

## Making space

| **Package to remove**     | **command**                           | **Space saved** |
| ------------------------- | ------------------------------------- | --------------- |
| acrobat                   | removepkg acrobat                     | 168MB           |
| frozen-bubble-data        | removepkg frozen-bubble-data          | 33.5MB          |
| tuxpaint                  | removepkg tuxpaint                    |                 |
| Plus the dependant data   | removepkg tuxpaint-data               | 26.7MB          |
| openoffice language packs | (follow the upgrade openoffice below) | \~200MB         |

**Commands**

List packages installed  
\# dpkg-query -l

package wildcard  
Show files installed by a package  
\# dpkg-query -L \<package name\>

![eeepc-tips-notes-1.doc](/workshops/eeepc-tips-notes-1.doc)

![](/workshops/eeepc-tips-notes-1.odt)
