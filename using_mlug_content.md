If you want to refer to any of our articles on your site you can using
an iframe. Please credit MLUG in the footer or somewhere visible on the
page with the iframe which links to our content. Thank you.

    <h1>MLUG article</h1>
    
    <iframe src="http://www.mlug.org.au/doku.php/tutorials/c/jumble?do=export_xhtmlbody" height="100%" width="900" style="border:0px #FFFFFF none;" scrolling="yes" >test</iframe>
    
    <footer>
    Article from <a href="http://www.mlug.org.au">www.mlug.org.au</a>
    </footer>
