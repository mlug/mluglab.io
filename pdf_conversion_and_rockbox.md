\*\*Monday 27th Feb, 2017 \*\*

| Topic                                     | Speaker     | Slides                                             |
| ----------------------------------------- | ----------- | -------------------------------------------------- |
| pdf-\>text-\>html-\>mp3-\>html with sound | Bob Parker. | <https://github.com/rlp1938/cli_editors>           |
| Rockbox on old mp3 players (short)        | Cameron     | [rockbox slides](/workshops/20170227-rockbox.pdf) |
