\*\* 28th August, 2017 \*\*

| Topic                 | Speaker      | Slides                                                |
| --------------------- | ------------ | ----------------------------------------------------- |
| KeePassX (lightning)  | Danny Robson | [keepassx\_slides](/workshops/20170828-keepassx.pdf) |
| Power (lightning)     | Michael Pope | [power\_slides](/workshops/20170828-power.pdf)       |
| Slackware (lightning) | Duncan Roe   |                                                       |
