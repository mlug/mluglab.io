***3. Adding a GUI interface to a bash script***  

-----

GUI interfaces can be used with bash scripts. The script below uses
*Zenity* to provide a GUI dialog for selecting an image to convert to
.pdf, a second GUI dialog for naming and saving the newly created .pdf
file and a third offering the option to delete the image file used to
create the .pdf file.

``` bash
#!/bin/bash
############################################################
# con2pdf-gui
# Intended to convert scanned document to pdf document 
# and provides gui interface for file selection.
# requires awk
# requires convert (from ImageMagick)
# requires dirname
# requires zenity to create gui file selection dialogs
# zenity requires GTK+
############################################################


input_file=`zenity --file-selection --title="Select an image file to convert to pdf"`
    
    if [ $? -eq 1 ]; then
            exit
        fi

working_dir=$(dirname $input_file)

cd $working_dir

output_file=`zenity --file-selection --save --title="Save .pdf file as..." --confirm-overwrite`

    if [ $? -eq 1 ]; then
        exit
    fi
    
convert $input_file $output_file

zenity --question --text="Do you want to delete the image file before you exit?"

    case $? in
        0) rm $input_file;;
        
        1) exit;;
    esac

# end of script
```

This script could be run by clicking an icon on a desktop and since the
interface is GUI there would be no need to open a terminal.

    input_file=`zenity --file-selection --title "Select an image file to convert to pdf"`

The first line of this script uses a zenity command within two backticks
to assign a file to the variable *input\_file*. The option
*file-selection* specifies that a file selection dialog box is required.
The option *--title* will create a title for the dialog using the text
enclosed in double quotes. If the user is running this script from an
icon on the desktop, The file selection dialog will open into
/home/\[user-directory\].

The script *con2pdf* in Section 1 either converted an image to a .pdf if
a target image file was entered after the command *con2pdf* or it
printed out the usage message and then returned to the command prompt if
no target file was entered. However, in the *zenity* file selection
dialog there are two user options, select a file or **Cancel**. It is
necessary after this zenity command executes to check the exit code to
see if the user has hit \*Cancel\* in which case the exit code would be
1. The following if statement should look familiar.

``` bash
    if [ $? -eq 1 ]; then
            exit
        fi
```

If a file has been selected and the user has pressed **OK** the script
will continue on to the next line after the if statement This line
creates the variable *working\_dir* and assigns it the path of
*$input\_file* using the command *dirname*. This variable is then used
to cd into the target file's directory. That directory now becomes the
script's *pwd* and the next *zenity* command will open a file selection
dialog in that directory.

``` bash
working_dir=$(dirname $input_file)

cd $working_dir
```

Next the script creates a variable, *output\_file*. This *zenity*
command is also a file selection dialog. The *--save* option is used
even though the script is creating a variable and not saving a file so
that the *--confirm-overwrite* option can be used. This is necessary
because the command *convert* will overwrite files without warning.

Another if statement is required in case the user hits the \*Cancel\*
button.

``` bash
output_file=`zenity --file-selection --save --title="Save .pdf file as..." --confirm-overwrite`

    if [ $? -eq 1 ]; then
        exit
    fi
```

The convert command is exactly the same as in the previous script.

``` bash
convert $input_file $output_file
```

However, in this script provides the option of deleting the image file
after the .pdf file has been created.

``` bash
zenity --question --text="Do you want to delete the image file before you exit?"

    case $? in
        0) rm $input_file;;
        
        1) exit;;
    esac
```

The *zenity --guestion* dialog prints the text into a pop up window that
provides two buttons, **Yes** and **No**. *Zenity* returns *0* if yes
and *1* if no.

A case statement follows which evaluates the exit code and will either
delete the file or exit the script accordingly. Case statements are
handy for this type of task and are generally used instead of if
statements when multiple options may be involved. Note the syntax as
well as that a case statement must close with *esac*.

-----

//There are other GUI dialog programs. KDE has *kdialog* which is based
on the Qt toolkit, there is also *dialog*, *xdialog*, //yad/ and others
(I'm sure). Their features and syntax vary but if you understand the
basics of how *zenity* was used in this script you shouldn't have any
trouble sorting out how to use one of the others//.

In the next section I''ll discuss a script I use on our print server to
shut it down if there is nobody using the lan.//

-----

**Cheers\!**
