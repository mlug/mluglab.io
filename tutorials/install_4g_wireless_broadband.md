# How to get 4g working under Linux

Thank you [jhend](http://forums.whirlpool.net.au/user/268905) from
whirlpool for the python script.

Telstra 4g Device (lsusb):

``` 
  0f3d:68aa Airprime, Incorporated
```

Have to upgrade kernel sierra wireless modules, compile these drivers  
[Sierra
update](http://mycusthelpadmin.net/SIERRAWIRELESS/_cs/AnswerDetail.aspx?sSessionID=8271670ADBE44815BFA546776724A658KBMXCSDU&aid=44)  
I'm using Linux kernel 2.6.38-13 on Linux Mint 11

Download drivers for your kernel  
<http://www.sierrawireless.com/resources/support/drivers/linux/v3.2_1740_kernel-2.6.38.directIP.tar>

Make dir and untar

``` 
  mkdir sierra
  tar xvf v3.2*tar
```

UNPLUG device

Compile

``` 
  make
  sudo make install
```

Check the sim card, there is a difference between bigpond & telstra sims
Ref: <http://forums.whirlpool.net.au/archive/1870382>

/home/angelo/bin/sierraoff

``` 
  #! /bin/sh
  sudo echo -e "AT+CFUN=0\r" > /dev/ttyUSB2
```

/home/angelo/bin/sierraon

``` 
  #! /bin/sh
  sudo echo -e "AT+CFUN=1\r" > /dev/ttyUSB2
```

Created this file /home/angelo/bin/sierraconnect and made it executable.

``` 
  #! /usr/bin/env python
  #
  import serial, time, sys, re
  #
  def s_read(lookfor, timeout_secs = 1):
    str1 = ''
    str2 = ''
    endtime = time.time() + timeout_secs
    while (str2.find(lookfor) < 0) and (time.time() < endtime):
      str1 = uport.read(999999)
      if len(str1): str2 += str1
      time.sleep(0.2)
    return str2
  #
  okresp = '\nOK\r\n'
  #
  port = '/dev/ttyUSB2'
  try:
    uport = serial.Serial(
      port,
      baudrate = 115200,
      bytesize = 8,
      stopbits = 1,
      parity = 'N',
      timeout = 0.5)
  except:
    print 'Cannot open ' + port + ', program stopping.'
    sys.exit(1)
  #
  args = sys.argv
  cid = '1'
  if len(args) == 2:
    temp = re.search('([0-9]*)', args[1])
    if temp:
      cid = temp.group(1)
      if cid == args[1]:
        temp = int(cid)
        if temp < 1 or temp > 16:
          print 'Profile ID ' + cid + ' is outside valid range 1 => 16'
          sys.exit(1)
      else:
        print args[1] + ' is not a not a valid profile (range 1 => 16)'
        sys.exit(1)
    else:
      print args[1] + ' is not a not a valid profile (range 1 => 16)'
      sys.exit(1)
  else:
    print 'No profile recognised on command line, using profile 1'
  time.sleep(1)
  uport.write('ATZ\r')
  s_read(okresp, 1)
  uport.write('ATE0\r')
  s_read(okresp, 1)
  #
  # ensure radio is on
  #
  uport.write('AT+CFUN?\r')
  outb = s_read('+CFUN: ', 3)
  if outb.find('+CFUN: 0') >= 0:
    uport.write('AT+CFUN=1\r')
    time.sleep(2)
    s_read(okresp, 10)
  #
  # connect using directIP method
  #
  uport.write('AT!SCACT=1,' + cid + '\r')
  time.sleep(2)
  s_read(okresp, 8)
  uport.close()
  sys.exit(0)
```

Run with profile 2 for bigpond (Angelo's SIM) or 3 for Telstra sim

``` 
  sudo bin/sierraconnect 2
```

Tried these commands:

``` 
  sudo dhclient -r
  sudo dhclient -nw wwan0
```

Should have a solid blue & green light on device (4G)

Procedure after reboot.

  - Start computer with modem not connected
  - Connect modem once logged in.
  - Wait until you see both solid blue and flashing green lights.
  - turn off with software

<!-- end list -->

``` 
   gksudo bin/sierraoff
```

  - start sierra modem

<!-- end list -->

``` 
   gksudo bin/sierraon
   gksudo bin/sierraconnect 2
```

  - Start dhclient

<!-- end list -->

``` 
   gksudo dhclient -nw wwan0
```

Job done\!

This should be in a easy to use GUI, better still this should work
within network manager\!

Here is the startup script I use and stored in /home/\<user\>/bin

``` 
  #!/bin/sh
  
  sudo /home/angelo/bin/sierraoff
  sleep 2
  sudo dhclient -r wwan0
  sudo dhclient -nw wwan0
  sleep 2
  # Run the first time to turn on the modem
  sudo /home/angelo/bin/sierraon
  sleep 5
  # Run a second time to start the connection
  sudo /home/angelo/bin/sierraconnect 2
  sleep 5
  zenity --info --text="Connection established"
```

I then made a link to this script on the users desktop with the
following command

``` 
  gksudo /home/<user>/bin/connect_4g
```


<hr>

Michael Pope 16/03/2012
