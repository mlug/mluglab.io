Here are some tutorials on the C programming language thanks to Robert
Parker

You can view it either as a PDF or HTML, here are the tutorials:

|                       |                                                                                 |                             |                                                                   |
| --------------------- | ------------------------------------------------------------------------------- | --------------------------- | ----------------------------------------------------------------- |
| Jumble                | [code](/tutorials/c/jumble.c.pdf) ![tutorial](/tutorials/c/jumble_explain.pdf) | [HTML](/tutorials/c/jumble) | ![code](/tutorials/c/jumble.gz) ![mydict](/tutorials/c/mydict.gz) |
| Extra Coding Examples |                                                                                 | [HTML](/tutorials/c/extra)  |                                                                   |

Coming soon: btsort, does an alpha search in 40% of the time that the
GNU 'sort' takes.
