***building a script with functions tht encodes, shrinks and burns
DVDs***

The previous tutorials were on the short side, heres one to make up for
my brevity. :^)

I often save movies and documentaries for viewing later and sometimes
want to burnthem onto a DVD to share with family and friends. In order
to burn the show I first have to crop anything extra before and after
the show plus any commercials in the middle. I use gopdit
<http://gopdit.ath.cx/> for the video editing and have it set to save
without rencoding so I'm left with the basic mpg-ps (program stream)
format that I saved the original show with.

I can use a basic script to save me the trouble of remembering (and
typing) a very long mencoder command that encodes the video to a good
DVD quality mpg-ps but the problem is that often I end up with a video
that is too big to fit on a DVD so I may have to shrink the video before
I burn it. Below is an example of a movie before and after encoding

    bash-4.1$ ls -l
    total 8849532
    -rwx------ 2 rick root 5068703744 Jun 17 06:18 encoded-hidden-blade.mpg
    -rwx------ 1 rick root 3937532284 Jan 12  2011 hidden-blade.mpg

**in progess**
