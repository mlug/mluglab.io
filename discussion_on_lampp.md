# LAMPP Stack

Host: Robert Moonen Date: 25th October, 2009

## Setting up a LAMPP Stack under Debian.

apt-get install apache2

    Will install:
    apache2 apache2-mpm-worker apache2-utils apache2.2-common libapr1
    libaprutil1 libpq5
    Suggested: apache2-doc apache2-suexec apache2-suexec-custom

apt-get install mysql-server

    Will install:
    libdbd-mysql-perl libdbi-perl libhtml-template-perl libnet-daemon-perl
    libplrpc-perl libterm-readkey-perl mysql-client-5.0 mysql-server
    Suggested: dbishell libipc-sharedcache-perl libcompress-zlib-perl tinyca

apt-get install php-pear

    Will install:
    php-pear php5-cli php5-common
    Suggested: php5-dev php4-dev

apt-get install php5-mysql

    Will install:
    php5-mysql

apt-get install libapache2-mod-php5filter

    Will install:
    apache2-mpm-prefork libapache2-mod-php5filter

apt-get install phpmyadmin libgd-tools libmcrypt-dev mcrypt
libgd2-xpm-dev libtool-doc autoconf automake1.9 gcj

The last step removes apache2-mpm-worker libapache2-mod-php5filter and
libgd2-noxpm, as they are included in other packages being installed.
