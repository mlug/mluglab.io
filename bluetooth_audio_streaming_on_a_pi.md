**27th May 2019**

| Talk                                      | Speaker      | Slides                                                               |
| ----------------------------------------- | ------------ | -------------------------------------------------------------------- |
| Bluetooth audio streaming to a Pi         | Michael Pope | [slides](/workshops/20190527-bluetooth_stream_audio_through_pi.pdf) |
| Problem solving roundtable, Show and tell | Discussion   |                                                                      |
| Website conversion progress               | Discussion   |                                                                      |
