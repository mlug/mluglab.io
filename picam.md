\*\* 26th June, 2017 \*\*

| Topic                                    | Speaker      | Slides                                         | Video                                         |
| ---------------------------------------- | ------------ | ---------------------------------------------- | --------------------------------------------- |
| Streaming and recording video using a PI | Michael Pope | [picam slides](/workshops/20170626-picam.pdf) | <https://www.youtube.com/watch?v=BwdlJMvZw58> |
