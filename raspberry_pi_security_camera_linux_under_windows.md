##### 25th Jan 2016

| Topic                        | Speaker      | Slides                                             |
| ---------------------------- | ------------ | -------------------------------------------------- |
| Raspberry PI Security Camera | Michael Pope | [slides](/workshops/20160125-security_camera.pdf) |
| Linux under Windows Part 2   | Duncan Roe   |                                                    |
