# Introduction to Lua programming

**Wednesday 26th June, 2013 @ 7pm  
**Address\*\* Muttis upstairs (118 Elgin st, Carlton VIC)

\*\* Introduction to Lua by David Burgess

www.lua.org  
www.luajit.org

![](/workshops/20130626-lua.odp)

Resources talked about  
http:*www.lua.org/pil/1.html  
http:*en.wikipedia.org/wiki/Help:Lua\_for\_beginners  
http://lua-users.org/wiki/LuaTutorial
