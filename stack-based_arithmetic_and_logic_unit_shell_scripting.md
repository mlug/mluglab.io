# Stack-based arithmetic and logic unit & shell scripting

  
**Meeting Topics for 25th June**

| Topic                               | Presenter           |
| ----------------------------------- | ------------------- |
| stack-based Arithmetic & Logic Unit | Duncan Roe          |
| Shell scripting                     | Surendra Kumar Anne |
| Describe your rig setup             | Open Forum          |
