# Remastersys: Make your own livecd

Date: 28th August  
Hosted By: Michael Pope

This is great for those wanting to keep a backup or create a distro.
There are two ways to run remastersys.

Create a live cd with your system and data intact

    remastersys backup

Create a distro with no data included

    remastersys distro

Presentation notes: [](/workshops/20090828_mlug.pdf)

## Cool things

Every week from now on we will have the cool things of this month. Here
are mine for this month.

Great reviews, tutorials and podcasts: <http://www.tuxradar.com>

KleanSweep, clean disk easily, web: <http://linux.bydg.org/~yogin>

SMPlayer - cool frontend to mplayer

Sockso music server, play your music over the internet great for
parties,  
<http://sockso.pu-gh.com/>

Spicebird, email client which does much more than thunderbird, contact,
calendar, tasks  
Web: www.spicebird.com/download

Super Grub Disk, mini boot disk (400KB download), easy menu driven grub
install/rescue.  
web: <http://supergrubdisk.org>

The software distribution system that works differently. rootz mounts
complete live distros over the web, and makes them available locally.
rootz - <http://vamosproject.org/rootz>

BG-Tiny Linux Bootdisk www.giannone.eu/bgtlb/current

``` 
  * You need at least a 386 machine (fpu not neccessary) with 4 mb ram to run BG-Tiny Linux Bootdisk.
  * Fits on floppy disk.
```

Blueproximity - locks up the screen when you walk away from your
computer with a bluetooth enabled phone. Good from a security aspect.
<http://blueproximity.sourceforge.net> stuffed up, doesn't really
unlock.

Made using Ruby on Rails <http://meetinbetween.us/>

<http://susestudio.com/> - customise your own distro and try it out
online.

Remastersys - Easy Live CDs for Ubuntu/Debian.  
<http://www.geekconnection.org/remastersys/remastersystool.html>

Ubuntu Customisation Kit- Even easier Live CD creation, Web:
<http://uck.sourceforge.net>

``` 
  * Works by customising a ISO image so you don't need to setup a system to create your live cd's
  * Unpacks ISO and allows you to install software, then repacks the ISO
```

Wammu, manage your phone through linux the easy way.  
Web: <http://wammu.eu>

Zile, cut down emacs for saving space.  
Web: www.gnu.org/software/zile

open font library - share fonts (Like open clip art)

OpenClipart which is usable: <http://testvm.openclipart.org/cchost/>

KDE 4.3 was released, which is just a polish of features.

Perl Audio Converter - <http://pacpl.sourceforge.net/> - batch convert
from one file format to another easier.
