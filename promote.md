## Help promote MLUG

We are always after new and exciting ways to promote our group.
Currently we have the following:

  - Regular posts on techevents.com.au of upcoming events
  - Facebook event/group
  - Website
  - Word of mouth.
  - Flyer, print this and post it up at your Work, University or tafe on
    their community board.

### Our flyer.

![](/mlug3.png)

![Full Page printable flyer (3 per A4) and inkscape original
file.](/mlug-flyer2.tar.bz2)

### Web Page button

![](/mlug_button.png)

If you would like to support MLUG please add this to your website:

    <a href='http://www.mlug.org.au'>
    <img src='http://www.mlug.org.au/lib/exe/fetch.php/mlug_button.png' alt='Melbourne Linux Users Group Button' border='0' />
    </a>

If you would like to improve the mlug flyer or create some new way to
promote MLUG please discuss this on our mailing list or IRC channel
\#mlug-au.
