# Next Meeting

## 24th June 2019

For this month only **we will be in the snug, not the dining function
room**.

It is the small room on the left as you walk from the dining area
through to the back garden.

**Address**

|                |                                                                     |
| -------------- | ------------------------------------------------------------------- |
| Place          | **The Great Northern Hotel**                                        |
| Address        | 644 Rathdowne St (corner of Pigdon St), Carlton North VIC 3054      |
| Pre-meeting    | 6pm, in the snug                                                    |
| Time for Talks | 7:30pm, in the snug                                                 |
| Website        | <http://gnh.net.au>                                                 |
| Phone          | (03) 9380 9569                                                      |
| Tram           | Routes 1 or 6 from Melbourne Central, get off at Pigdon st, Carlton |

## Volunteer for a talk

Feel like doing a talk? Please volunteer on our mailing list or email me
directly at [danny@nerdcruft.net](danny@nerdcruft.net).

## Talks

**24th June 2019**

| Talk                               | Speaker        | Slides |
| ---------------------------------- | -------------- | ------ |
| Shell access via injection         | Damien Buttler |        |
| Kernel bug hunting with git bisect | Duncan Roe     |        |

**27th May 2019**

| Talk                                      | Speaker      | Slides                                                               |
| ----------------------------------------- | ------------ | -------------------------------------------------------------------- |
| Bluetooth audio streaming to a Pi         | Michael Pope | [slides](/workshops/20190527-bluetooth_stream_audio_through_pi.pdf) |
| Problem solving roundtable, Show and tell | Discussion   |                                                                      |
| Website conversion progress               | Discussion   |                                                                      |

**29th April 2019**

| Talk       | Speaker      | Slides                                        |
| ---------- | ------------ | --------------------------------------------- |
| bashrc fun | Danny Robson | [slides](/workshops/20190429-bashrc_fun.pdf) |

**25th March 2019**

| Talk               | Speaker      | Slides                                         |
| ------------------ | ------------ | ---------------------------------------------- |
| Multiboot USB      | Michael Pope | [slides](//20190325-usb_multiboot.pdf)        |
| Jitsi, Matrix, IRC | Open         | [slides](//20190325-jitsi_matrix_and_irc.pdf) |

**25th February 2019**

| Talk                                               | Speaker      |
| -------------------------------------------------- | ------------ |
| PiTooth - Raspberry Pi as a Bluetooth/​USB adapter | Danny Robson |
| Kodi + Netflix, 3dprinters, etc                    | Michael Pope |

**28th January 2019**

| Talk                         | Speaker      |
| ---------------------------- | ------------ |
| Really complicated car clock | Michael Pope |
| LCA2019 report               | Duncan Roe   |

**30th December 2018**

No meeting as per usual for December. Enjoy your break\!

**26th November 2018**

| Talk          | Speaker    |
| ------------- | ---------- |
| Show and tell | Open floor |

For this month only we are in the 'snug' (still at The Great Northern)
due to a booking clash.

You are encouraged to bring along, or have someone else bring along,
something you have found interesting to show off in an unstructured
discussion.

**24th September 2018**

| Talk                  | Speaker                        |
| --------------------- | ------------------------------ |
| Danny Robson & Remote | Jitsi Conferencing Experiments |

**27th August 2018**

| Talk         | Speaker                         |
| ------------ | ------------------------------- |
| Michael Pope | Setting up a serial console     |
| General      | Open Floor Problem Solving Q\&A |

**30 July 2018**

| Talk         | Speaker                            |
| ------------ | ---------------------------------- |
| Timothy Rice | An introduction to filesystem ACLs |

**25 June 2018**

| Talk            | Speaker                    |
| --------------- | -------------------------- |
| Darren Wurf     | Blockchain Data Structures |
| Malcolm Herbert | The `jq` JSON processor    |

**28th May 2018**

| Talk                                                 | Speaker      |
| ---------------------------------------------------- | ------------ |
| command line double entry accounting with ledger-cli | Michael Pope |
| A Raspbery Pi as an ethernet-wifi bridge             | Danny Robson |

**30th April 2018**

| Talk                         | Speaker      |
| ---------------------------- | ------------ |
| Static Blogging with Pelican | Danny Robson |

**28th March 2018**

| Talk                            | Speaker      |
| ------------------------------- | ------------ |
| NVMe                            | Duncan Roe   |
| Raspberry Pi air quality sensor | Michael Pope |

**26th February 2018**

| Talk         | Speaker      |
| ------------ | ------------ |
| Gentoo Linux | Danny Robson |
| nftables     | Duncan Roe   |

**29th January 2018**

| Talk                               | Speaker      |
| ---------------------------------- | ------------ |
| Spectre & Meltdown Discussion      | All @ Dinner |
| borg backup                        | Danny Robson |
| csmanager for NextCloud management | Bob Parker   |

**27 November - Cancelled**

This months meeting is cancelled as most people cannot make it.

**30 October 2017**

| Talk                                           | Speaker      |
| ---------------------------------------------- | ------------ |
| KRACK WPA vulnerability pre-meeting discussion | Everyone     |
| gnuplot                                        | Rick Miles   |
| Low powered x86 board (lightning)              | Michael Pope |

**Feel free to volunteer for a talk on our mailing list.**

## Future meetings

Dates for 2017

| Date        | Talk \#1 | Talk \#2 |
| ----------- | -------- | -------- |
| 29-Jan-2018 |          |          |
| 26-Feb-2018 |          |          |

Please show your interest on our mailing list or email me directly

## Talk Preparation

Normal talks go for 45minutes, lightning talks go for 15minutes. If
there a lot of questions please save them for after the talks. There is
room for two full talks and one lightning. Please let me know which
length your talk is when volunteering.

The talks are performed in the function room.

The LED TV supports HDMI up to 1920x1080.

## Live Demos

MLUG is known for doing live demos and it's something which makes our
group unique. We would like to continue this tradition so if you do a
live demo as part of your talk please make sure it fits within the time
limit. If it does take a long time you can setup the demo and perform
the demo after all the talks have been done.

## Sponsors

A big thank you to our sponsors;

|                                                                  |                                   |
| ---------------------------------------------------------------- | --------------------------------- |
| ![<http://affinityvision.com.au/>](//affinity_vision_banner.jpg) | IT support & Domain Name Services |

## Join the MLUG mailing (Google Group)

The MLUG mailing list is great place to keep up to date on Linux news
and events and discuss Linux/open source related questions.


<table style="background-color: #fff; padding: 5px;" cellspacing=0>
  <tr><td style="padding-left: 5px;font-size: 125%">
  <b>Melbourne Linux User Group</b>
  </td></tr>
  <tr><td style="padding-left: 5px">
  <a href="http://groups.google.com/group/mlug-au" target="_blank">Join the MLUG Google Group</a>
  </td></tr>
</table>

## IRC Channel

'mlug-au' at freenode.  
<irc://irc.freenode.net/mlug-au>

## Do you want to help make the MLUG website better?

  - Evaluate alternatives to dokuwiki, set them up and show them at MLUG
    meetings either before or after.
  - Mock up a better design or suggest a better menu structure.
  - Updates to our other groups listing, I would love this to be
    complete.
  - Create tutorials / content for our website, such as cheat sheets or
    handy hints.

Refer to our discussion at the start of the year at: [rolling distros &
MLUG 2012
improvements](/workshops/rolling%20distros%20&%20MLUG%202012%20improvements)

#### Mailing list tips

[top vs bottom posting
explained](http://www.idallen.com/topposting.html)  
[Why bottom posting is better than top
posting](http://www.caliburn.nl/topposting.html)

## Main MLUG Contact

It's best to head over to the mailing list for any questions, discussion
or announcements. If you are having problems related to MLUG and you are
not on mailing list you can contact:  
  
|Contact |**Danny Robson**|

|         |                     |
| ------- | ------------------- |
| Email   | danny@nerdcruft.net |
| Twitter | @dcro398            |

## New to Linux

<http://www.makeuseof.com/tag/help-make-year-linux-desktop/>

## Notebooks with Linux pre-installed or no OS

<http://linuxpreloaded.com/>

\*\*Check compatibility before buying a notebook:
<http://www.linlap.com/> \*\*

\*\* Hardware guide \*\* <http://www.linux-hardware-guide.com/>

<http://www.techdrivein.com/2010/09/7-providers-of-pre-installed-linux.html>

\*\* Support GNU/Linux/BSD Vendors \*\*

MLUG is not connected to these companies in any way. These are just
companies we have found that are selling notebooks/computers with the
option of Linux pre-installed or no OS installed.

Australia  
<http://www.linuxnow.com.au/> - Lots of options and lots of distros on
offer (Melbourne based)  
<http://www.horize.com> -- Horize notebooks, better specs than some of
the stores  
<http://www.logicalblueone.com.au> - Offer Ubuntu or No OS, competitive
prices (Horize only)  
http://www.vgcomputing.com.au  
<http://www.pioneercomputers.com.au/> - Good range, plenty of options.  
<http://www.elx.com.au/cat/systems> - Sydney based but can deliver.

Overseas  
<http://www.system76.com/> - They now deliver to Australia  
http://www.linuxcertified.com/linux\_laptops.html  
<http://www.emperorlinux.com/>

## MLUG workshop extras

The new extras that we talk/work on at meetings are:

  - Working on improving the MLUG group such as website, mailing list,
    services, venue selection, getting the word out about mlug.
  - Cool Things this month (everyone who comes gets a chance to tell us
    what's cool this month in the world of Linux and Open Source)
  - Too Hard Basket. Everyone has a task which is too hard to complete,
    this is a chance to get fresh input on your "too hard basket" item.

## Other

Linux useful commands: <http://www.pixelbeat.org/cmdline.html>

http://www.computerbank.org.au  
**You can contact us via our Mlug [mailing list](mailing_list)**  
  

## OLD

**[OLD BLOG](http://www.linux-mlug.blogspot.com/)**
