# AWS deployment of WebPageTest|Daniel Cross

**Wednesday 28th August, 2013 @ 7pm (pre-meeting downstairs at 6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

## AWS deployment of WebPageTest

Daniel covered the process of getting the WebPageTest tool deployed onto
an Amazon Web Service with a mixture of Python scripts and Json
configuration templates.

## Voice recognition

Duncan went over some open source voice recognition software he has been
looking at.

## Network & RSI tools

Sven went over two great tools he uses:

NTM (network traffic monitor) - Good for 3G data plans so you don't go
over your limit.  
workrave - Prevent the chance of RSI

## xdotool & others

Michael gave a quick intro to some of the tools he uses for testing hard
to test software or for workarounds when nothing else will do the trick.

|            |                                |
| ---------- | ------------------------------ |
| xdotool    | keyboard/mouse actions         |
| xsel       | manipulate the X selection     |
| zenity     | collect info from user         |
| gdevilspie | window matching & size setting |
| wmctrl     | control windows shape/size     |
| gnee       | GUI Record sessions            |
| xtail      | a clean tail                   |
