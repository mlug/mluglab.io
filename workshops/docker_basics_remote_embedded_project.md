\*\* Next Meeting \*MONDAY\* 31th August \*\*

| Topic                                                 | Presenter    | Slides                                                     |
| ----------------------------------------------------- | ------------ | ---------------------------------------------------------- |
| Docker Basics                                         | Michael Pope | [slides](/workshops/20150831-docker_basics.pdf)           |
| Overview of remote embedded project (Lightening talk) | Peter Stone  | [slides](/workshops/20150831-greatcow-python-sockets.pdf) |

Extra docker slides

<http://de.slideshare.net/robertreiz/docker-39958028?utm_medium=social&utm_source=twitter&utm_campaign=docker-2681022>
