# Zombies game in C and Migrating to RAID1

|                   |                                                                                    |
| ----------------- | ---------------------------------------------------------------------------------- |
| **Date:**         | 25th July, 2012                                                                    |
| **Pre-meeting**   | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                               |
| **Meeting**       | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\]                |
| **Hosted By:**    | Tim Rice and Nitin Sharma                                                          |
| **Meeting Topic** | Zombies (an ncurses game written in C), Migration of / to RAID1 on existing system |

## Zombies

Tim created a Zombie game to brush up on his C skills. In this talk he
went through the game and the code.

Git repository

``` 
git clone https://github.com/cryptarch/zombies.git

```

Links mentioned in the talk.

RogueBasin:

``` 
  http://roguebasin.roguelikedevelopment.org/
```

RogueBasin has a nice 15 step how-to for building a roguelike game.

The second link is to a tutorial on ncurses in C:

``` 
  http://www.bigwebmaster.com/General/Howtos/NCURSES-Programming-HOWTO/
```

## RAID1 Migration

![RAID1 slides with commands](/workshops/mlug_raid1_existing_system.odp)
