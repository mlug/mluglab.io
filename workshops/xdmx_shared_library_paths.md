\*\*Monday 27th June, 2016 \*\*

| Topic                          | Speaker      | Slides                                                                             |
| ------------------------------ | ------------ | ---------------------------------------------------------------------------------- |
| Networked multi-screen         | Michael Pope | [xdmx\_slides](/workshops/20160627-xdmx_networked_multi-monitor.pdf)              |
| Shared library path resolution | Danny Robson | [shared\_library\_slides](/workshops/shared_library_path_resolution_in_linux.pdf) |
