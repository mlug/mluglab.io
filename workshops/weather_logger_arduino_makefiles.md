\*\*Monday 30th Jan, 2017 \*\*

| Topic                         | Speaker      | Slides                                                                 |
| ----------------------------- | ------------ | ---------------------------------------------------------------------- |
| Weather logger                | Rick Miles   | ![weather\_logger\_slides](/workshops/bpro-ws.odp)                     |
| Arduino Makefile (lightening) | Michael Pope | [arduino\_makefile\_slides](/workshops/20170130-arduino_makefile.pdf) |

## Linux Conf links of interest

<https://www.youtube.com/watch?v=T3NKMi_f21Q][/sys/class/gpio/Parking Brake: Hacking my car>

<https://www.youtube.com/watch?v=FMqydRampuo][Rust 101>

<https://www.youtube.com/watch?v=m4YTFI2kd8g][Making Night in the Woods Better with Open Source>
- Game under open source

<https://www.youtube.com/watch?v=3BNNY6_r3tQ][Network Protocol Analysis for IoT Devices>
- Jonathan Oxer

<https://www.youtube.com/watch?v=OJUnRMtK9Nw][I am your user. Why do you hate me?>
- Donna Benjamin

<https://www.youtube.com/watch?v=7QVgzVcoV_M][The dangerous, exquisite art of safely handing user-uploaded files>
- Tom Eastman

<https://www.youtube.com/watch?v=qKJz4aPubNI][The Internet of Scary Things - tips to deploy and manage IoT safely>
- Christ Biggs

<https://www.youtube.com/watch?v=Utjx2h7nz-g][Are you a bad enough dude to take down your network?>
- Tom Cuthbert

<https://www.youtube.com/watch?v=LaRjThUGXLE][The Internet of Houses: Whare Hauora>
- Brenda Wallace & Amber Craig

<https://www.youtube.com/watch?v=AmMYWD2Zbso][Sharing the love: making games with PICO-8>
- John Dalton (PocketChip PICO-8 game programming)

<https://www.youtube.com/watch?v=ZLdvDnxvJI8][Let's put wifi in everything.>
- Brenda Wallace

<https://www.youtube.com/watch?v=d8kfva6G0c4][Porting Games To Linux> -
Cheese

<https://www.youtube.com/watch?v=X4sLpGdGQjc][Turtles all the way down - Thin LVM + KVM tips and Tricks>
- Steven Ellis

<https://www.youtube.com/watch?v=4oq4ursOnlw][Lightweight inventory management with Pallet Jack>
- Karl-Johan Karlsson

<https://www.youtube.com/watch?v=k3brfCZSFiQ][JavaScript is Awe-ful> -
Katie McLaughlin
