\*\* 31st July, 2017 \*\*

| Topic                                | Speaker      | Slides                                 |
| ------------------------------------ | ------------ | -------------------------------------- |
| GPG/PGP key signing session & basics | Danny Robson | [slides](/workshops/20170731-gpg.pdf) |
| System76 notebook demo (lightning)   | Andre Vidic  |                                        |
