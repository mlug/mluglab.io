# Rolling release distros & MLUG 2012 improvements

|                    |                                                                                                                              |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------- |
| **Date:**          | 25th Jan, 2012                                                                                                               |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                                                                         |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\]                                                          |
| **Hosted By:**     | Michael Pope & Audience (discussions)                                                                                        |
| **Workshop Topic** | video recording MLUG to the masses, Improvements to MLUG (suggestions will be welcomed), comparing 'rolling release distros' |

## Summary email

**If you would like to discuss anything in this email please join the
mailing list and have your say.**

Last nights meeting we went over a lot of different suggestions on how
to improve MLUG this year. Here is a summary: Please provide your
suggestions to the following and if you would like to get involved in
any of these projects.

Linux Conf AU videos are available from:
<http://mirror.internode.on.net/pub/linux.conf.au/2012/>

### Video

We would like to stream live video of the presentations with a twitter &
IRC client for live interaction. I went to Linux Conf AU and saw a talk
by Tim Ansell on this topic. We came to the conclusion that we should
start off small, maybe just record the audio first and present that
along with the slides on our website.

We have to think about the following questions

  - Where are we going to host it (Someone suggested Linux Australia has
    space, or Bruno's home server)?
  - What license should the content be under? (I suggested one of the
    Creative Commons licenses but don't know which one would be
    appropriate, we came to the conclusion that we wanted a license
    which gave us freedom to do what we want with it, but not be used
    for commercial purposes)
  - What format should the content be in? OGG & Youtube perhaps.
  - What hardware do we require and can we do it for nothing? David has
    a web came with good resolution and a good quality semi directional
    external microphone.

I suppose we can think about the streaming another day. If we
concentrate firstly on the audio, maybe we could stream the audio as
that would be easier.

Someone suggested that Linux Australia supplies grants for these kind of
projects. I suppose one of us has to talk to Linux Australia and see
what the story is in regards to grants, hosting and anything else which
may help us.

If you are interested in finding out more about Tim Ansell's talk you
can watch it here:
<http://mirror.internode.on.net/pub/linux.conf.au/2012/Making_video_streaming_interactive_heckling_user_groups_from_the_clouds.ogv>

And join the googlegroup 'streamtime' as Tim Ansell will be publishing
his full documentation on his streaming solution.

### Car pooling

There was talk about car pooling to and from meetings. This raised some
interesting questions:

  -  How many people would be interested in getting a lift to and from
    the MLUG meetings?
  -  Is transport to the meetings currently a problem for you?
  -  Is transport stopping you from coming to the meetings at all?

If we find that there is a big demand for transport we will then have to
think about how we are going to implement a safe structure in which
people can ask for a lift and find someone they trust to take them to
and from the meeting. For now we are interested to see if this is a
problem or not.

### Website

We talked about where we are at with the website. It's been running for
over 4yrs on Dokuwiki and it's time to review if this is the best
solution:

Current problems:

  -  It's hard to update
  -  It doesn't look great.
  -  The structure is hard to find our talks or too cumbersome to find
    different levels of talks, ie: beginner/advanced

Some suggestions:

  -  Replace the whole thing.
  -  Fix the current dokuwiki up and continue using it.
  -  build our own
  -  hold a competition to design a new theme for the MLUG website.
  -  look at what other LUGs have done and question them.
  -  use ikiwiki (<http://ikiwiki.info/>) as there is no upgrade process
    and it's pretty easy to use.
  -  convert emacs org-mode into html (problem here is not everyone can
    then edit it easily).
  -  tiddlywiki <http://www.tiddlywiki.com/> - This one is portable, you
    can download it and put it on to a USB stick
  -  Keith suggested we slim down the website and make it faster to
    download on poor connections.
  -  Rethink the page titles and split talks into 'beginner,
    intermediate, advanced' to make it easier to find.

Questions for MLUG members

  -  How did you find out about MLUG?
  -  Do you find it easy to navigate the current website?
  -  What wiki have you used in the past? share your experience.
  -  Are looks important? Should the MLUG website look better and how
    are we going to do that?

### Beginners talks

Bruno suggested that we hold some beginners talks to get more people to
our meetings. So we are now going to have two talks every month, one
beginners talk & one advanced. We started have two talks at the end of
last year so this shouldn't change the length of the night much.

### Other suggestions

Other suggestions throughout the night

  -  Daren was interested in a joint project.
  -  Access to WIFI at the premises. This gets brought up a few times
    throughout the year and the I have spoken to the manager in the
    past. Currently there is no access to the WIFI and people usually
    use their phones to get some internet. We have found this works
    quite well. There is a chance to get access to the WIFI but we would
    have to chip in. I don't like dealing with money and want to keep
    MLUG free so people can just rock up and have a good time so I think
    this is out of the question. I'm open to suggestions or arguments
    here for or against. I find that if you really need internet at MLUG
    for getting a package/code or access to a server we can work
    something out for those issues pretty easy. I don't see a big need
    to all start chipping in and getting a WIFI plan, even if it was a
    dollar each, it's the hassle of collecting money.

### Help promote MLUG

Help promote MLUG to your friends, uni students and work colleges. We
have flyers and web buttons which you can post on community boards or
around your work (make sure you get permission before doing so).

Go here to find out more: **[promote](/promote)**

If you have any suggestions and you were not at the meeting please speak
up now. If you could answer some of the questions in this email so we
can gather a greater understanding of our members needs that would be
great.

## Rolling Release Distros

Note: It was pointed out that Debian 'Testing' is not a true rolling
release as it is frozen for a few months before a stable release.
Unstable is a rolling release and is not as unstable as one might think.

[Slides](/workshops/201201_mlug.pdf)
