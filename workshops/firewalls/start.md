# Firewalls

Date: 30th October  
Hosted By: Michael Pope & Robert Moonen  
We cover two firewalls;  
\- Smoothwall Express3 (Linux Based)

1.  pfSense 1.2.2 (FreeBSD Based)

[](/workshops/firewalls/20091031_mlug_firewalls.pdf)

[pfSense 2.0 Info](/workshops/94_pfsense_2_0_and_beyond_bsdcan_09.pdf)

[smoothwall\_pictures](/workshops/firewalls/smoothwall_pictures)
