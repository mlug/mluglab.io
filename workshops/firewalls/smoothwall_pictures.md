## Smoothwall pictures

Page 1 ![](/workshops/smoothwall_main_page.png)
![](/workshops/status_page.png)

Page 2 ![](/workshops/status_page_advanced2.png)
![](/workshops/status_page_advanced3.png)
![](/workshops/status_page_advanced4.png)
![](/workshops/status_page_advanced5.png) ![](/workshops/traffic1.png)
![](/workshops/traffic2.png)

Page 3 ![](/workshops/services_page.png)
![](/workshops/an_instant_messaging_proxy_.png)
![](/workshops/email_pop3_proxy_with_clamav.png)
![](/workshops/voip_sip_proxy_.png) ![](/workshops/dhcp_server_1.png)
![](/workshops/dhcp_server_2.png)

Page 4 ![](/workshops/dynamic_dns_client.png)
![](/workshops/smoothwall_remote_access.png)
![](/workshops/keeping_the_time_synchronised.png)

Page 5 ![](/workshops/networking_pages.png)
![](/workshops/port_forwarding_.png)
![](/workshops/outgoing_allowed_1.png)
![](/workshops/outgoing_allowed_2.png)
![](/workshops/pinholes_from_dmz.png)

Page 6 ![](/workshops/logs_page.png) ![](/workshops/ip_block.png)
![](/workshops/logs_intrinsic_to_smoothwall.png)
![](/workshops/checking_for_newly_installed_computers.png)
![](/workshops/analyzing_the_web_proxy_.png)
