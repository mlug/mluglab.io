**Meeting Topics for 30th April**

| Topic                                | Presenter   | Slides/Info                                        |
| ------------------------------------ | ----------- | -------------------------------------------------- |
| Sikuli graphical automation software | Darren Wurf | <http://dwurf.github.io/mlug-sikuli-presentation/> |
| Slackbuild scripts                   | Duncan Roe  | <http://slackbuilds.org/>                          |
