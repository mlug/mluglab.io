**27th August 2018**

| Talk                        | Speaker      | Slides                                            |
| --------------------------- | ------------ | ------------------------------------------------- |
| Setting up a serial console | Michael Pope | [slides](/workshops/20180827-serial_console.pdf) |
