# Workshops

Here is the list of workshops which we have completed at MLUG  
|27th May 2019|[bluetooth audio streaming on a
pi](/workshops/bluetooth%20audio%20streaming%20on%20a%20pi)|

|                     |                                                                                                                                             |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| 25th March 2019     | [multiboot usb and jitsi, matrix and irc](/workshops/multiboot_usb_and_jitsi,_matrix_and_irc)                                   |
| 25th February 2019  | [Kodi\_Netflix\_3dPrinters\_PiTooth](/workshops/kodi_netflix_3dprinters_pitooth)                                                            |
| 28th January 2019   | [Complicated\_clock\_car\_computer\_lca2019\_report](/workshops/complicated_clock_car_computer_lca2019_report)                              |
| 29th October 2018   | [NanoPI\_M4\_IPTables\_GeoBlocking](/workshops/nanopi_m4_iptables_geoblocking)                                                              |
| 27th August 2018    | [Setting up a serial console](/workshops/setting_up_a_serial_console)                                                               |
| 28th May 2018       | [PiFi and ledger-cli](/workshops/pifi_and_ledger-cli)                                                                                   |
| 30th April 2018     | [Static sites and xdotool](/workshops/static_sites_and_xdotool)                                                                       |
| 26th February 2018  | [nftables and Gentoo](/workshops/nftables_and_gentoo)                                                                                   |
| 29th January 2018   | [csmanager and borgbackup](/workshops/csmanager_and_borgbackup)                                                                         |
| 25th September 2017 | [MIDI\_and\_openshot](/workshops/midi_and_openshot)                                                                                         |
| 28th August 2017    | [keepassx\_power\_and\_slackware](/workshops/keepassx_power_and_slackware)                                                                  |
| 31st July 2017      | [gpg and system76](/workshops/gpg_and_system76)                                                                                         |
| 26th June 2017      | [picam](/workshops/picam)                                                                                                                   |
| 29th May 2017       | [cross compile and apps on chromebooks](/workshops/cross_compile_and_apps_on_chromebooks)                                         |
| 24th April 2017     | [github and virtualisation for gaming](/workshops/github_and_virtualisation_for_gaming)                                             |
| 30th March 2017     | [csv2anki and package manager for everyone](/workshops/csv2anki_and_package_manager_for_everyone)                                 |
| 27th February 2017  | [pdf conversion and rockbox](/workshops/pdf_conversion_and_rockbox)                                                                   |
| 30th January 2017   | [weather logger & arduino makefiles](/workshops/weather_logger_arduino_makefiles)                                                 |
| 28th November 2016  | [Bash with data and Mechanical keyboards](/workshops/bash_with_data_and_mechanical_keyboards)                                     |
| 31st October 2016   | [Bash on Windows, Uboot and Munin](/workshops/bash_on_windows,_uboot_and_munin)                                                   |
| 26th September 2016 | [puppet & chocolatey, runindent and sfl](/workshops/puppet_chocolatey,_runindent_and_sfl)                                       |
| 30th August 2016    | [exwm and 3d printing](/workshops/exwm_and_3d_printing)                                                                               |
| 25th July 2016      | [CHIP, BOINC and tox](/workshops/chip,_boinc_and_tox)                                                                                 |
| 27th June 2016      | [Xdmx & shared library paths](/workshops/xdmx_shared_library_paths)                                                               |
| 30th May 2016       | [Datascience & Syncthing](/workshops/datascience_syncthing)                                                                           |
| 18th April 2016     | [Cyanogenmod & Linux on cheap notebooks](/workshops/cyanogenmod_linux_on_cheap_notebooks)                                       |
| 21st March 2016     | [VMware performance and photography](/workshops/vmware_performance_and_photography)                                                   |
| 29th February 2016  | [docker-compose & post motion detection](/workshops/docker-compose_post_motion_detection)                                         |
| 25th January 2016   | [Raspberry PI security camera & Linux under windows](/workshops/raspberry_pi_security_camera_linux_under_windows)           |
| 30th November 2015  | [System Z, OpenStreetMap and Linux under windows 2](/workshops/system_z,_openstreetmap_and_linux_under_windows_2)             |
| 26th October 2015   | [rcorder, PostgreSQL & Docker](/workshops/rcorder,_postgresql_docker)                                                               |
| 31st August 2015    | [Docker Basics & Remote embedded project](/workshops/docker_basics_remote_embedded_project)                                     |
| 27th July 2015      | [Hansard & Markov, HTML5 audio and linux on Chromebooks](/workshops/hansard_markov,_html5_audio_and_linux_on_chromebooks) |
| 29th June 2015      | [electric\_eye & MPD](/workshops/electric_eye_mpd)                                                                                    |
| 25th May 2015       | [ssh-proxy & crypt](/workshops/ssh-proxy_crypt)                                                                                       |
| 27th April 2015     | [tarsnap & vpn](/workshops/tarsnap_vpn)                                                                                               |
| 30th March 2015     | [btrfs backup & flirc.tv](/workshops/btrfs_backup_flirc.tv)                                                                         |
| 23rd February 2015  | [Arcade PI & Git](/workshops/arcade_pi_git)                                                                                         |
| 28th January 2015   | [Openstreet offline](/workshops/openstreet_offline)                                                                                       |
| 29th October 2014   | [Self signed SSL & RPI security cam](/workshops/self_signed_ssl_rpi_security_cam)                                             |
| 24th September 2014 | [GNU autotools](/workshops/gnu_autotools)                                                                                                 |
| 27th August 2014    | [blogs & emacs](/workshops/blogs_emacs)                                                                                               |
| 30th July 2014      | [VPS & duplicates](/workshops/vps_duplicates)                                                                                         |
| 25th June 2014      | [stack-based arithmetic and logic unit & shell scripting](/workshops/stack-based_arithmetic_and_logic_unit_shell_scripting) |
| 28th May 2014       | [mixnets & vagrant and Ansible](/workshops/mixnets_vagrant_and_ansible)                                                           |
| 30th April 2014     | [Sikuli & Slackbuild scripts](/workshops/sikuli_slackbuild_scripts)                                                                 |
| 26th March 2014     | [packer.io & SQL Basics](/workshops/packer.io_sql_basics)                                                                           |
| 26th February 2014  | [Arch, smokeping & wireless cameras](/workshops/arch,_smokeping_wireless_cameras)                                                 |
| 29th January 2014   | [Raspberry PI Captive access point](/workshops/raspberry_pi_captive_access_point)                                                   |
| 27th November 2013  | [backup, netbsd, john the ripper & MySQL workbench](/workshops/backup,_netbsd,_john_the_ripper_mysql_workbench)             |
| 30th October 2013   | [old file cleaner & lightning talks](/workshops/old_file_cleaner_lightning_talks)                                               |
| 25th September 2013 | [Web scalability & PostScript](/workshops/web_scalability_postscript)                                                               |
| 28th August 2013    | [AWS deployment of WebPageTest](/workshops/aws_deployment_of_webpagetest)                                                             |
| 31st July 2013      | [Tour de Chateau & LVM](/workshops/tour_de_chateau_lvm)                                                                           |
| 26th June 2013      | [lua](/workshops/lua)                                                                                                                       |
| 29th May 2013       | [Ansible](/workshops/ansible)                                                                                                               |
| 24th April 2013     | [Linux on ARM & Pizza netmap 2.0](/workshops/linux_on_arm_pizza_netmap_2.0)                                                   |
| 27th March 2013     | [Community & XBMC on PI](/workshops/community_xbmc_on_pi)                                                                         |
| 27th February 2013  | [SSH tricks and screen](/workshops/ssh_tricks_and_screen)                                                                             |
| 23rd January 2013   | [Python performance, openstack and hybrid graphics](/workshops/python_performance,_openstack_and_hybrid_graphics)                 |
| 28th November 2012  | [Q CuBox Proxmox](/workshops/q_cubox_proxmox)                                                                                           |
| 26th September 2012 | [VM/build automation](/workshops/vm/build_automation)                                                                                     |
| 29th August 2012    | [Specialist scientific and data mining software](/workshops/specialist_scientific_and_data_mining_software)                       |
| 25th July 2012      | [Zombies game in C and migrating to RAID1](/workshops/zombies_game_in_c_and_migrating_to_raid1)                               |
| 27th June 2012      | [tvconfig and raspberrypi](/workshops/tvconfig_and_raspberrypi)                                                                         |
| 30th May 2012       | [pdf tools and large Arduino project overview](/workshops/pdf_tools_and_large_arduino_project_overview)                         |
| 2nd May 2012        | [LUKS and IPv6](/workshops/luks_and_ipv6)                                                                                               |
| 28th Mar 2012       | [organisation and gpg setup](/workshops/organisation_and_gpg_setup)                                                                   |
| 29th Feb 2012       | [python gtk & Amahi media server](/workshops/python_gtk_amahi_media_server)                                                     |
| 25th Jan 2012       | [rolling distros & MLUG 2012 improvements](/workshops/rolling_distros_mlug_2012_improvements)                                   |
| 30th November 2011  | [Programming tools, emacs and vim](/workshops/programming_tools,_emacs_and_vim)                                                     |
| 26th October 2011   | [OpenCOG and WIIMOTES](/workshops/opencog_and_wiimotes)                                                                                 |
| 28th September 2011 | [LXC virtualisation and chef-solo](/workshops/lxc_virtualisation_and_chef-solo)                                                       |
| 24th August 2011    | [File system snapshots and openoffice macros](/workshops/file_system_snapshots_and_openoffice_macros)                             |
| 27th July 2011      | [zsh tips and tricks](/workshops/zsh_tips_and_tricks)                                                                                 |
| 29th June 2011      | [PXE tricks](/workshops/pxe_tricks)                                                                                                       |
| 25th May 2011       | [PDF manipulation](/workshops/pdf_manipulation)                                                                                           |
| 27th Apr 2011       | [openstreetmap](/workshops/openstreetmap)                                                                                                   |
| 30th March 2011     | [Introduction to ChromeOS](/workshops/chrome_os)                                                                                            |
| 23rd February 2011  | [file systems filibuster](/workshops/file_systems_filibuster)                                                                           |
| 24th November 2010  | [Arduino Hacking](/workshops/hacking_on_arduino)                                                                                            |
| 27th October 2010   | [Video editing, joomla and OSGUI](video_editing_joomla_and_osgui)                                                                           |
| 29th September 2010 | [Sorting program](/workshops/sorting_program)                                                                                             |
| 25th August 2010    | [how\_i\_converted\_my\_office\_to\_linux](/workshops/how_i_converted_my_office_to_linux)                                                   |
| 29th July 2010      | [BYO\_scripts](/workshops/byo_scripts)                                                                                                      |
| June 2010           | technical discussion (Mainly about the ATO and lack of Linux support and telstra bashing about the withdrawal of their linux mirrors.)      |
| May 2010            | technical discussion                                                                                                                        |
| 30th April 2010     | [Review of 4 different cloning techniques](/workshops/disk_cloning)                                                                         |
| 26th March 2010     | No Meeting.                                                                                                                                 |
| 26th February 2010  | LTSP                                                                                                                                        |
| 29th January 2010   |                                                                                                                                             |
| 30th October 2009   | [Two well known Firewalls turn up the heat](/workshops/firewalls/start)                                                                     |
| 25th September 2009 | [Discussion on setting up a LinuxApacheMysqlPhP stack under Debian/Ubuntu](/workshops/discussion_on_lampp)                              |
| 28th August 2009    | [Make a live CD from your system using remastersys](/workshops/remastersys)                                                                 |
| 31st July 2009      | [How to encrypt a usbkey](/workshops/encrypt_usb)                                                                                         |
| June 2009           | discussion only                                                                                                                             |
| 29th May 2009       | [Setting up Apache Virtual Hosts](/workshops/apache_virtual_hosts)                                                                          |
| 24th April 2009     | [Using a GIT repository](/workshops/git)                                                                                                    |
| 27th March 2009     | [Setting up and using VirtualBox to create virtual machines](/workshops/virtualbox)                                                         |
| 27th Feb 2009       | [Setting up and using InternetRelayChat](/workshops/irc)                                                                                    |
| 29th August 2008    | [Virtualisation](/workshops/virtualisation)                                                                                                 |

|2
