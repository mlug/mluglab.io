|                           |                                                                 |
| ------------------------- | --------------------------------------------------------------- |
| **Date:**                 | 31st July                                                       |
| **Time:**                 | 7pm (Pre meeting 6pm at Jawa Bar 297 victoria st)               |
| **Location:**             | Workshop held at Computer bank                                  |
| **Hosted By:**            | Michael Pope                                                    |
| **Workshop Topic**        | Encrypting your USB stick                                       |
| **Workshop Requirements** | cryptsetup package and a blank up USB which you want encrypted. |

[encrypted file system](/workshops/encrypted_file_systems.pdf) ![Cool
Stuff](/workshops/cool_stuff.pdf)
