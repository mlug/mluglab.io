# tvconfig and raspberrypi

|                   |                                                                     |
| ----------------- | ------------------------------------------------------------------- |
| **Date:**         | 27th June, 2012                                                     |
| **Pre-meeting**   | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**       | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**    | Michael Pope                                                        |
| **Meeting Topic** | TV tuner config GUI & raspberryPI demo                              |

## tvconfig

tvconfig makes installing TV cards under Linux very easy.

I created it for many reasons;

  - Easy for non-technical people to install TV cards under Linux
  - Saves me time when I setup MythTV boxes for people
  - Nice and fast to use.

![tvconfig slides](/workshops/20120627-tvconfig.odp)

Checkout the project at: <http://github.com/map7/tvconfig>

Feel free to add any configurations you have and send me a pull request
and I'll merge it into the live project.

## Raspberry PI

Michael gave a short explanation of the RaspberryPI, what it's made of
and how it works.

<http://www.raspberrypi.org/>
