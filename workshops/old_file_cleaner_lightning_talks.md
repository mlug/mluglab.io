# Old File Cleaner

**Wednesday 30th October, 2013 @ 7pm (pre-meeting downstairs at 6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

## Old File Cleaner

Bob Parker went through his old file cleaner tool which helps find all
sorts of junk on the system and remove it.

<https://github.com/rlp1938/oldfiles>

## Lightning talks

Darren Wurf - <http://dwurf.github.io/lightning-talk-python-utils/#/>
