**Topics for \*MONDAY\* 23rd Feb**

|                           |              |                                                                                                                          |
| ------------------------- | ------------ | ------------------------------------------------------------------------------------------------------------------------ |
| Topic                     | Person       | Slides/Links                                                                                                             |
| Git                       | Tim Rice     | [github: sgit](https://github.com/cryptarch/sgit) [gitnotes](https://github.com/cryptarch/gitnotes/blob/master/index.md) |
| Arcade Machine using a Pi | Michael Pope | [PI arcade](/workshops/20150223-mlug-pi-arcade.pdf)                                                                     |
