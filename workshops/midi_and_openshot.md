\*\* 25th September, 2017 \*\*

| Topic              | Speaker | Slides                                        |
| ------------------ | ------- | --------------------------------------------- |
| MIDI Keyboard talk | Andrew  | ![midi\_slides](/workshops/20170925-midi.odp) |
| Openshot           | Cameron |                                               |
