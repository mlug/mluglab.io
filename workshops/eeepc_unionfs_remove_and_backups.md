eeepc Backup & then remove UnionFS

Date: 25th July, 2008 Hosted By: Michael Pope & John Coombes

This is building upon last months hacking eeepc as a request from
regular MLUG attendees.

## Backup & Restore the eeepc

Backup before you remove the unionfs and after any major changes on a
regular basis. This saves a lot of time in the future.

This backup procedure will work with any linux machine not just eeepc's.
It's the most simple way to backup your system. It takes more room and
you cannot move it from disk to disk unless they are totally identical
but this is ok for the eeepc.

### Backup

Insert your backup media (SD or USB) (make sure you have at least 2GB
free on that device)

Create a directory /home/user/bin

Start kedit and paste in the following script:

    echo `date`
    echo "Backup usually takes about 45minutes..."
    sudo dd if=/dev/sda | gzip -c9 > /media/MMC-SD/partition1/eeepc-backup_dd.gz
    echo "Finished "
    echo `date`

Change the /media/MMC-SD/partition1 to your USB or SD device and
partition name.

Save this file in /home/user/bin/backup

Now when you start up a new terminal you should be able to run backup.

### Restore

To restore you will either need a USB cdrom drive or a bootable USB
stick. Refer to our first workshop [linux on
usb](/workshops/linux%20on%20usb).

Now you can insert the backup media (SD or USB)

Mount that media device and type the following command.

\*\* Warning this will over ride all data on your eeepc \*\*

    gunzip -c /<path to backup media>/eeepc-backup_dd.gz | dd of=/dev/hdc

Note that /dev/hdc could be different depending on what you have booted
from.

Under Backtrack 3 it's /dev/hdc, under System Rescue it's /dev/sda. You
can check by unpluging your backup media and typing 'df -h' and looking
at the size to determine which device is your internal HD.

## Removing the unionfs

The wiki has heaps of information on how to remove the unionfs. If you
have any questions let me know on the mailing list and I'll post the
answers up here.

[Remove UnionFS](http://wiki.eeeuser.com/howto:removeunionfs)
