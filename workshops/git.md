# GIT

Hosted by: Robert Postill  
Date: 24th April, 2009

![Presentation](/workshops/lug-presso-vcs.odp)

![Git Config Files](/workshops/git_config_files.tar.gz)  
Included in the git config files are:  
gitconfig.txt (Move this to \~/.gitconfig and edit the fields within)  
gitignore.txt (This is an example of what to ignore for a rails
application. This should be called .gitignore and lives inside the root
directory of your project directory)
