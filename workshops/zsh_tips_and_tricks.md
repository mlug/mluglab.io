# ZSH tips and tricks

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 27th July, 2011                                           |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Mick & Bob                                                          |
| **Workshop Topic** | ZSH tips and tricks & step through some C code with Bob.            |

Michael covered the basic advantages of using ZSH over other shells and
how easy it is to start using and gaining some of the advantages with
minimal effort.

Plugin used 'oh-my-zsh':  
<https://github.com/robbyrussell/oh-my-zsh>

References (in order of preference):  
http:*www.tuxradar.com/content/z-shell-made-easy  
http:*zsh.sourceforge.net/Guide/zshguide.html  
http:*www.rayninfo.co.uk/tips/zshtips.html  
http:*physos.com/2010/09/24/my-custom-zsh-prompt/  
http:*grml.org/zsh/zsh-lovers.html  
http:*stevelosh.com/blog/2010/02/my-extravagant-zsh-prompt/  
<http://grml.org/zsh/zsh-lovers.html>

Presentation notes:  
![ZSH Presentation](/workshops/20110727-zsh_slides.odp)  
[ZSH Presentation pdf format](/workshops/20110727-zsh_slides.pdf)
