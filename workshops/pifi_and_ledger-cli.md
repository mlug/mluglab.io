**28th May 2018**

| Talk                                     | Speaker      | Slides                                  |
| ---------------------------------------- | ------------ | --------------------------------------- |
| A Raspbery Pi as an ethernet-wifi bridge | Danny Robson | [slides](/workshops/20180528-pifi.pdf) |
| xdotool                                  | Michael Pope | slides                                  |
