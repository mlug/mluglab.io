**Topics for \*MONDAY\* 30th March**

| Topic                         | Presenter    | Slides                                                           |
| ----------------------------- | ------------ | ---------------------------------------------------------------- |
| btrfs backup scripts          | Danny Robson | ![btrfs slides](/workshops/20150330-talk-danny-btrfs-backup.zip) |
| Flirc.tv remote control setup | Michael Pope | [flirc.tv slides](/workshops/20150330-flirc.pdf)                |
