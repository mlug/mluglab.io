# backup\_netbsd\_john\_the\_ripper\_mysql\_workbench

**Wednesday 27th November, 2013 @ 7pm (pre-meeting downstairs at
6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

**Meeting Topics**

| Topic                                  | Presenter       |
| -------------------------------------- | --------------- |
| Backup gem                             | Michael Pope    |
| NetBSD Install                         | Malcolm Herbert |
| john the ripper & spamming the tab key | Cameron Dawdy   |
| MySQL workbench with MariaDB           | Duncan Roe      |

The last meeting to be held at Muttis as it closed soon after.
