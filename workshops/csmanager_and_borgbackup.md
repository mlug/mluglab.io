**29th January 2018**

| Talk                               | Speaker      | Slides                                        |
| ---------------------------------- | ------------ | --------------------------------------------- |
| Spectre & Meltdown Discussion      | All @ Dinner |                                               |
| borg backup                        | Danny Robson | [slides](/workshops/20180129-borgbackup.pdf) |
| csmanager for NextCloud management | Bob Parker   |                                               |
