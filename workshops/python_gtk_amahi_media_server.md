# Python GTK & Amahi media server

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | 29th Feb, 2012                                                      |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Bruno Parisi & Bianca Gibson                                        |
| **Workshop Topic** | Amahi home media server & Python and the GUI                        |

## Python GTK

Bianca went over the tips and tricks of setting up Python GTK
applications. Below is her well documented code for a sudoku game.

![](/workshops/sudoku.zip)

## Amahi

Bruno described the best parts of Amahi. Mainly what distro to setup
Amahi on;

|             |                                                                       |
| ----------- | --------------------------------------------------------------------- |
| Fedora 13   | Everything worked and was free                                        |
| \*Fedora 14 | Paid services came in but free plugins still worked                   |
| Fedora 15   | Support for this version was skipped                                  |
| Fedora 16   | Most free plugins didn't work any more and it's harder to get working |

\* = Best choice

<http://www.amahi.org/>

It should work on Ubuntu/Debian systems as well.

It's best for inhouse media streaming to TV's and PS3's. There is no
transcoding with in this program you have to use a 3rd party plugin for
that.
