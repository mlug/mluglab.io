# VPS through Digital Ocean & duplicates

  
**Meeting Topics for 30th July**

| Topic                     | Presenter |                                         |
| ------------------------- | --------- | --------------------------------------- |
| VPS through Digital Ocean | Ronaldo   |                                         |
| duplicates program        | Bob       | <https://github.com/rlp1938/Duplicates> |
