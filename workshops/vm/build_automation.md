# VM/build automation

|                   |                                                                     |
| ----------------- | ------------------------------------------------------------------- |
| **Date:**         | 26th September, 2012                                                |
| **Pre-meeting**   | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**       | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**    | David Schoen                                                        |
| **Meeting Topic** | VM/build automation                                                 |

[VM\_Build\_automation Audio
Recording](http://www.mlug.org.au/recordings/MLUG_2012-10-02_David_Schoen_VM_Build_Automation.ogg)

[Refer to David's blog for more
details](http://lyte.id.au/2012/10/02/vm-build-automation-with-vagrant-and-veewee/)
