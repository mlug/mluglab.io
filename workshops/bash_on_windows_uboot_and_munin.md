\*\*Monday 31st October, 2016 \*\*

| Topic                                   | Speaker        | Slides                                 |
| --------------------------------------- | -------------- | -------------------------------------- |
| Bash on Windows                         | Darren Wurf    |                                        |
| Uboot (lightning)                       | Rick Miles     | [uboot slides](/workshops/u-boot.pdf) |
| Monitoring and Notifications with Munin | James Pinkster | [](/workshops/munin-2016-10-31.pdf)   |
