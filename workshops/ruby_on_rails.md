# Ruby on Rails

Date:29th Feb 2008  
Hosted by: Rob Postill  
This workshop was presented on 29th Feb 2008, by Robert Postill. The
workshop went for 2.5 hrs and Rob did a fantastic job of keeping on
topic

**Workshop Requirements**  
For this workshop please have these packages installed: Postgres (I use
8+, but you could use less).  
PGadmin III (I use 1.6.3) - <http://www.pgadmin.org/download>  
Packages for ruby, rails, postgres (gem)  
Ruby 1.8.6 -
ftp:*ftp.ruby-lang.org/pub/ruby/1.8/ruby-1.8.6-p111.tar.gz  
Rubygems 1.0.1 -
http:*rubyforge.org/frs/download.php/29548/rubygems-1.0.1.tgz  
gem install rails  
gem install postgres-pr  
  
  
![](/workshops/20080229-rails-lug-presso.odp)

If you have any problems please post your questions on the mailing list.
