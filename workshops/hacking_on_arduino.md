# Hacking on Arduino

|                |                  |
| -------------- | ---------------- |
| **Date:**      | 24th Nov, 2010   |
| **Hosted By:** | George Patterson |

Brief history of Arduino and how to begin hacking with it.

![5fENF81g5ns](/youtube\>large/5fenf81g5ns)

![Arduino Slides](/workshops/arduino_mlug.odp)
