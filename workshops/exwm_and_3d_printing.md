|                              |              |                                         |        |
| ---------------------------- | ------------ | --------------------------------------- | ------ |
|                              | Topic        | Speaker                                 | Slides |
| exwm - tiled windows manager | Michael Pope | [slides](/workshops/20160829-exwm.pdf) |        |
| 3d printing                  | Frans        |                                         |        |
