# Specialist scientific and data mining software

|                   |                                                                     |
| ----------------- | ------------------------------------------------------------------- |
| **Date:**         | 29th August, 2012                                                   |
| **Pre-meeting**   | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**       | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**    | Lauchlin Wilkinson                                                  |
| **Meeting Topic** | An intro to specialist scientific and data mining software packages |
