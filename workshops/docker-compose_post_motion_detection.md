##### 29th February, 2016

| Topic                 | Speaker      | Slides                                                   |
| --------------------- | ------------ | -------------------------------------------------------- |
| docker-compose        | Darren Wurf  | [slides](/workshops/docker_compose.pdf)                 |
| post motion detection | Michael Pope | [slides](/workshops/20160229-post_motion_detection.pdf) |
