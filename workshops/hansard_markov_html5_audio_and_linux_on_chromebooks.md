\*\* Next Meeting \*MONDAY\* 27th July \*\*

| Topic                                         | Presenter    | Slides                                                                       |
| --------------------------------------------- | ------------ | ---------------------------------------------------------------------------- |
| (lightening) Hansard and Markov chains        | Danny Robson | [markov\_slides](/workshops/experiments_with_markov_chains_and_hansard.pdf) |
| (lightening) HTML5 MP4 sound                  | Duncan Roe   |                                                                              |
| (lightening) Installing linux on a Chromebook | Michael Pope | [chromebook\_slides](/workshops/20150727-linux_on_chromebooks.pdf)          |
