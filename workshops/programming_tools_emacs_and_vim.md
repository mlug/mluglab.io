# Programming tools, emacs and vim

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 30th November, 2011                                       |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Duncan Roe & Michael Pope                                           |
| **Workshop Topic** | Progamming tools & Emacs configurations compared                    |

Duncan went through a set of tools which he has used to boost his
efficiency in his job as a programming over the last 20 odd years. Most
of these commands were based around minimal keystrokes, usually single
character commands. It was also important for his tools to keep previous
output on the screen.

Duncan's programming tools -
<https://github.com/duncan-roe/command_line_tools.git> (bin and source
subdirectories)  
![.bashrc file within a tar](/workshops/bashrc.tar) (Note: make a backup
of your .bashrc file first)

Michael discussed his emacs configuration which uses the marmalade
packages to minimise configuration files and compiles for the users
system allowing quick startup of emacs. He went on to discuss some of
the advanced features such as spell check comments/strings,
autocomplete, debug features, database interaction and more.

Presentation notes:  
[20111130\_emacs.pdf](/workshops/20111130_emacs.pdf)

Michael's emacs configuration is up on his github account at:  
<https://github.com/map7/simple_emacs>

Rich Healey prepared a talk on vim, here are the notes  
<https://github.com/richoH/mlug_vim_talk>
