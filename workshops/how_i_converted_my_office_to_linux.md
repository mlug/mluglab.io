# How I converted my office to Linux

|                |                                                   |
| -------------- | ------------------------------------------------- |
| **Date:**      | 25th Aug, 2010                                    |
| **Time:**      | 7pm (Pre meeting 6pm at Jawa Bar 297 victoria st) |
| **Location:**  | Workshop held at Computer bank                    |
| **Hosted By:** | Michael Pope                                      |

Presentation: ![](/workshops/how_i_converted_my_office_to_linux.ppt)

References:  
[Existing Windows system to VirtualBox
converter](http://www.vmware.com/products/converter/)  
[ITWire link to my
article](http://www.itwire.com/opinion-and-analysis/open-sauce/41329-a-win-lin-situation-moving-a-small-office-over-to-linux)  
<http://www.virtualbox.org/wiki/Migrate_Windows>

## Cool stuff

Remmina  
\- Remote desktop client handles: NX, VNC, RDP, SFTP, SSH, XDMCP  
Google command line <http://code.google.com/p/googlecl/> (Now in
repository).  
\- Control: Blogger, Youtube, Picasa, Docs, Contacts & Calendar (may
have bugs).  

OpenChange - Exchange alternative with native support for Outlook  
http:*www.openchange.org  
Remote control your desktop using your Google phone -
http:*www.gmote.org/  
Gow – The lightweight alternative to Cygwin -
http://wiki.github.com/bmatzelle/gow/
