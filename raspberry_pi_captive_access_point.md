# Captive access point + discussions

**Wednesday 29th January, 2014 @ 7pm (pre-meeting downstairs at 6pm)**  
  
**Address**  
La Spaghetteria Ristorante  
Address: (upstairs) 132 Lygon St, Carlton VIC 3053  
Phone:(03) 9663 4333  
[Directions from train station](http://goo.gl/maps/hjVYs)  
  

## Raspberry PI Captive access point

Cameron performed his talk on setting up a Raspberry PI a festival for
mapping and information.

What I'd really like for next year's mark 3 version (I may need some
help to do this, not sure):

  -  Proper tile rendering using mapnik - so I can upload some changes
    to the raspberry pi's hosted GIS database and the appropriate
    changes will be rendered on the pi. - And support for pre-rendering
    a whole bunch of files on my desktop computer beforehand.
  -  A weather forecast page that can scrape
    <http://openweathermap.org/API> before being deployed somewhere
    offline for a week. - Openweathermap supports giving you a weather
    forecast given some coordinates in xml / json, both formats I know
    little about how to parse.
  -  Some way to grab a feed from <http://emergency.vic.gov.au/> and
    render it as a layer on my leaflet based map (with something to
    specify to the user when that data was last updated).
  -  Implement a tilt servo mechanism so the camera can be remotely
    adjusted up and down.
  -  Mesh networking support would also be nice, and some way of syncing
    forum posts and GIS data across similarly equipped raspberry pis.

Timelapse taken with the system  
<https://www.youtube.com/watch?v=CKE0rq6hzpA>

## Discussions

|                                                      |               |         |
| ---------------------------------------------------- | ------------- | ------- |
| Suggestion                                           | Volunteer     | Month   |
| own services, aka: like ownCloud.                    |               |         |
| ansible                                              | Damien Butler | May     |
| virtualisation                                       |               |         |
| getting off google services to own rig.              |               |         |
| reproduce services onto a cheap rig, like dlink.     |               |         |
| setting up mail server (librelist)                   |               |         |
| portable servers (debian, KVM, LVM, Raid)            |               |         |
| mSATA                                                |               |         |
| secure gateways (DHCP, firewall, easy, policy based) |               |         |
| code of ethics                                       |               |         |
| developer guidelines                                 |               |         |
| peer review group                                    |               |         |
| LAMP/dokuwiki                                        |               |         |
| security/cryptography                                |               |         |
| BSD                                                  |               |         |
| peer to peer secure chat                             |               |         |
| tor/like tor                                         |               |         |
| nodejs servers                                       |               |         |
| nginx                                                |               |         |
| running web servers on phones                        |               |         |
| Arch                                                 | Ben Kaiser    | Feb     |
| encryption                                           |               |         |
| SQL basic/tips and tricks                            | Darren        | Feb     |
| basic where to put things                            |               |         |
| FreeNAS, but like Dropbox replacement                |               |         |
| Dropbox replacements                                 |               |         |
| DB                                                   |               |         |
| map reproduce                                        |               |         |
| PostGIS                                              | Darren        | June    |
| map renderings, pump out tiles                       |               |         |
| bluetooth proximity                                  |               |         |
| mongoDB                                              |               |         |
| robotics using JS                                    |               |         |
| x2goserver                                           |               |         |
| low power devices                                    |               |         |
| slackbuild templates testing script.                 |               |         |
| android ROM                                          |               |         |
| tiling window managers                               |               |         |
| 4 camera wireless security system on a budget        | Michael       | Feb/Mar |
