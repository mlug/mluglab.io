# GNU Autotools & Systemd vs SysV

24th September

## An overview of GNU autotools by Danny Robson

[autotools.pdf](/workshops/20140924-autotools.pdf)

Notes: - creates Makefile and is the standard way of working - standard
configure, make, make install. - great for a heap of distros and
different architecture - great for anything but windows. - bit more
painful for Windows - it's old, but it's reliable & stable. - good for
specific languages c,C++,objective C, erlang. Not so much python, ruby,
lisp, perl.

\- configure.ac - where is the compiler, where are the tools, uses M4
language - autoconf - creates configure shell script. - configure - how
we are going to build things - Makefile.am - what do you want to build,
where are those files

Simplified - You only really need to worry about these two files

1.  configure.ac - tools
2.  makefile.am - where your source is.

\- autoscan can create your configure.ac, it's not perfect - autoproject
- same but from debian, it's complicated.

\- autoconf uses any file with a '.in' extension.

\- AC\_SEARCH\_LIBS allows you to find libraries depending on the distro

\- pkg-config

\- autoheader - lots of defines - autoconf-archive - finds a lot of libs
- gettext - I18n, compiles into different languages. - libtool - Huge
library tools. Helps with many systems for library.

Further Reading - Don't bother with the 'Goat book' - Read GNU manuals,
really good. - man pkg-config, lists all options you need.

## Systemd vs Sys-V

It was mostly a one sided arguement for systemd. Join the mailing list
for an ongoing debate between the two.
