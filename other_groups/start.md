# Other Computer Related Groups

These are other groups which meet regularly around Melbourne and worth
checking out.  
Please verify the meeting date and time on the groups website before
attending.  

| Group                                                                                                                      | Date\[1\]                                                                           | Time                      | Type                                       | Location                                                       |
| -------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ------------------------- | ------------------------------------------ | -------------------------------------------------------------- |
| [MelbournePUG (Python)](http://wiki.python.org/moin/melbournepug)                                                          | 1st Monday                                                                          | 6pm                       |                                            | Inspire9 at 1/41 Stewart Street in Richmond                    |
| [Linux Users Victoria](http://www.luv.asn.au/)                                                                             | 1st Tues                                                                            | 7pm                       | Formal Presentations                       | Melb Uni                                                       |
| [Melbourne Patterns](http://melbournepatterns.org/)                                                                        | 1st Wed                                                                             | 6:45pm                    | Lectures about different types of patterns | Thoughtworks Level 15, 303 Collins Street, Melbourne, VIC 3000 |
| [2600](http://www.2600.org.au/meetings.php)                                                                                | 1st Friday                                                                          | 6:30pm                    | Security Meetings                          | ReVault Bar Melb                                               |
| <http://www.austrek.org/>                                                                                                  | 1st Saturday                                                                        | 1pm - 5pm                 | Star Trek                                  | Northcote Town Hall                                            |
| [MelbPC](http://member.melbpc.org.au/~linux)                                                                               | 2nd monday                                                                          | 7pm - 9:30pm (except jan) |                                            | Moorabbin Meeting Room                                         |
| [Victoria .NET Dev SIG](http://www.victoriadotnet.com.au/vic-events/vic-technical-events.aspx)                             | 2nd Tuesday                                                                         | 5:30pm                    | Presentation                               | Microsoft Theatre, Level 5, 4 Freshwater Place, Southbank      |
| [MXPEG](http://tech.groups.yahoo.com/group/melbourne_xp_enthusiasts/)                                                      | 2nd Tues                                                                            | 6:30pm                    | Programming Design                         | Lower ground floor, 45 Collins Street, Melbourne               |
| [Perl Group](http://melbourne.pm.org/)                                                                                     | 2nd Weds                                                                            | 6:30pm                    |                                            | Remasys Pty Ltd, Level 1 180 Flinders St MELBOURNE VIC 3121    |
| [PHP Group](http://phpmelb.org/)                                                                                           | [Mailing list](https://osdc.securityprotected.net/lists/archives/main.phpmelb.org/) |                           |                                            | 580 St Kilda Rd, Melb.                                         |
| [MNUG (Novell)](http://www.mnug.org.au/meetings/)                                                                          | 2nd Thursday                                                                        | 6.15pm                    |                                            | Novell - 8th Floor 574 St.Kilda Rd, Melbourne                  |
| [Melbourne Wireless](http://melbourne.wireless.org.au/)                                                                    | 2nd Friday                                                                          | 7:30pm                    |                                            | Hawthorn Scout Hall                                            |
| [Melbourne free software interest group](http://www.softwarefreedom.com.au/free-software-melb/)                            | 3rd Thurs                                                                           | Discussion                | Melb Library.                              |                                                                |
| [Melbourne scala user group](http://groups.google.com/group/scala-melb)                                                    | 4th Monday                                                                          | 6:30pm                    | Presentation                               | Level 2, 476 St Kilda Rd                                       |
| [Melbourne Ruby](http://ruby.org.au/) [Mailing list (meeting announcements)](http://groups.google.com/group/rails-oceania) | Last Thurs of the month                                                             | 6:30pm                    | Presentations                              | Check mailing list.                                            |
| [Dorkbot](http://projects.dorkbot.org/dorkbot-wiki/dorkbotmelbwiki)                                                        | Last Sunday                                                                         | 4pm                       | Embedded                                   | Fitzroy                                                        |
| [Web Standards](http://webstandardsgroup.org/meetings/)                                                                    | Wed or Thurs Check Site                                                             |                           |                                            |                                                                |
| [OSDC](http://www.osdc.com.au/osdclub/)                                                                                    | ???                                                                                 |                           |                                            |                                                                |
| [VicFUG](http://www.vicfug.au.freebsd.org/)                                                                                | Infrequent                                                                          |                           | FreeBSD                                    |                                                                |
| <http://www.port80.asn.au/>                                                                                                |                                                                                     | Web                       |                                            |                                                                |
| [Functional programmer's union](http://groups.google.com/group/fpunion)                                                    | fortnightly?                                                                        |                           |                                            |                                                                |
| <http://infrastructurecoders.com/>                                                                                         |                                                                                     |                           |                                            |                                                                |

|                                        |                      |
| -------------------------------------- | -------------------- |
| <http://www.meetup.com/Mel-Nightowls/> | Programming at night |

Extra groups can be found on the Perl group site:
<http://perl.net.au/wiki/Melbourne>

Melbourne JS  
<http://melbjs.com/>

Hacker space (Electronics and robots etc.)  
<http://www.hackmelbourne.org/>

Refer: http:*perl.net.au/wiki/Melbourne  
http:*www.meetup.com/  
DevOps  
<http://www.meetup.com/devops-melbourne/>

[Hack Melbourne
Space](http://groups.google.com/group/connected-community-hackerspace?pli=1)

1.  Dates relate to the month
