# MLUG...

Is a group of Linux enthusiasts in Melbourne, Victoria, Australia who
meet on the last Monday evening of the month from 6pm to 9:30pm. The
meeting starts at 7pm.

For the location refer to our "[meeting\_location](meeting_location)"
page

The main way that Mlug members communicate is via their Mlug mailing
list.

## Who are MLUG members?

Mlug members range from Linux coders and System admins, to ordinary end
users who just use the Linux GUI for their desktop. Although if you are
not a Linux user yet, but you are interested in learning. Then you are
most welcome to join. Note; no direct support for the MS-Windows O/S is
offered - unless it is related to interoperability with Linux.

The group is Linux/Unix based without focus to any specific
distribution.

## MLUG Thank-you List

Our thanks to the following members for their support of MLUG (in no
particular order).

  - [Affinity Vision Australia](http://affinityvision.com.au/) - IT
    support & Domain Name Services
  - [Digitech Corporation](http://dtcorp.com.au) - Domain Name & Hosting
  - Christian Lehrran - Mail List Hosting 2006-\>2008
  - Bill Farrow - Website editor
  - Robert Parker - Website editor
  - John Coombes - Website editor
  - George Patterson - IRC admin
  - Mark Campbell - Website editor , Previous Administrator, Website
    Hosting
  - Robert Postill - Website editor
  - John Simmons, [Genesis Networks](http://www.genesis.net.au) -
    Contributor
  - Tim Mullins - Camera man. <http://www.osgui.com>
  - Michael Pope - Website administration, Administrator

## Other

**[NEW BLOG](http://melbourne-linux-users-group.blogspot.com/)**
