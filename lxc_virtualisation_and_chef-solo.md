# LXC Virtualisation & Chef-solo

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 28th September, 2011                                      |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Michael Pope & Darren Wurf                                          |
| **Workshop Topic** | Building systems with chef-solo, Virtualisation with LXC            |

### Chef

![Presentation slides (basic
overview)](/workshops/20110928_mlug_chef.pdf)  
[Chef-solo indepth](/workshops/chef-solo.pdf)

### LXC Virtualisation

![LXC Presentation slides](/workshops/lxcslides.odp)

![LXC scripts](/workshops/scripts.tar.gz)
