**25th March 2019**

| Talk               | Speaker      | Slides                                         |
| ------------------ | ------------ | ---------------------------------------------- |
| Multiboot USB      | Michael Pope | [slides](//20190325-usb_multiboot.pdf)        |
| Jitsi, Matrix, IRC | Danny Robson | [slides](//20190325-jitsi_matrix_and_irc.pdf) |

Multi-boot code: <https://github.com/map7/write_usb>
