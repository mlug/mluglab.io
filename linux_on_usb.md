# Linux on USB Workshop

Date: 25th Jan 2008  
Hosted By: Michael Pope  
EDIT: I've found if you use a program called
[UNetBootin](http://unetbootin.sourceforge.net/) then you can make
bootable usb drives using ISOs through a GUI. This works on
Windows/Linux/etc.

EDIT: More info:
<http://minipc.org/safepup/index.php?file=Completing%20HP-USB%20Steps.htm>

Download
[feather-linux](http://ftp.nluug.nl/ftp/pub/os/Linux/distr/feather/feather-0.7.4-usb.zip)

In this workshop I will be showing you how to install linux on to a USB
Memory stick through the command line. This procedure can be used for
many different linux distros. There are easier ways of installing linux
on a usb memory stick, such as using the Kubuntu live CD, but what
happens if the distro you want doesn't have an easy wizard.  
  
Before you get started it's a good idea to backup all the data and look
at the original specs of your USB Memory stick.  
  
For this demonstration I will be using the following USB Memory stick:  
  
Corsair Voyager GT 4GB  
Original specs of my memory stick

``` 
   Disk /dev/sdb: 4110 MB, 4110417920 bytes
   241 heads, 32 sectors/track, 1040 cylinders
   Units = cylinders of 7712 * 512 = 3948544 bytes
  
      Device Boot      Start         End      Blocks   Id  System
   /dev/sdb1   *           1        1041     4014064    b  W95 FAT32
```

I will be performing all these commands under my eeepc which runs
Xandros (based on debian 4). These are very common tools and any linux
system will be able to perform these actions, providing you have
installed a few packages.  
  
The packages which are required are mtools and syslinux (this is under
debian, they maybe called something different on other distros).  
  
Step by step procedure:  
Enter USB stick in and mount (eeepc will auto mount)  
  
Check what the device is called, you can either use the 'df -h' command
or 'dmesg' to find this information. On my computer it's /dev/sdc.  
  
I chose to use cfdisk (curses fisk) but you can equally use fdisk.  
\# cfdisk /dev/sdc

1.  Hit 'delete' on the partitions
2.  Hit 'new', 'primary', go with default size
3.  Hit 'type', choose '06', 'write', type 'yes', enter
4.  Now Hit 'Bootable', 'write', type 'yes', enter
5.  'Quit'

  
Note: If cfdisk fails to load then use 'fdisk' and delete each partition
on your USB drive, then try cfdisk again and it should load.  
  
This will create a FAT16 (type 06) Bootable USB drive. I'm using FAT16
as this can be read on any machine without additional
packages/software.  
  
Now you need to unmount and format the partition in dos format.  
\# umount /dev/sdc1

    # mkdosfs /dev/sdc1

Mount the USB

    # mount /dev/sdc1 /mnt

Copy the contents across as a normal user.

    $ unzip feather-0.7.4-usb.zip /mnt

Install the boot loader application

    # apt-get install mtools syslinux

Install the boot loader onto the Memory stick

    # syslinux /dev/sdc1

### Note:

Not all computers boot from USB so you may have to test this on another
computer. You may have to play around with syslinux so that it works
properly, best version is 2.11 if you can compile that.  
Upgrading to the latest requires 'mtools'  
  
Compiling notes syslinux

    # apt-get install nasm mingw-gcc

Still doesn't compile

### References

<http://forum.eeeuser.com/viewtopic.php?id=6107> -- Backtrack 3 on eee  
<http://www.pendrivelinux.com/2006/03/25/puppy-linux-on-usb/>
