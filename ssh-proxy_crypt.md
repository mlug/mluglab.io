**\*Topics for \*MONDAY\* 25th May at The Dan**\*

| Topic                    | Presenter       |                                            |
| ------------------------ | --------------- | ------------------------------------------ |
| ssh-proxy                | Malcolm Herbert | [slides](/workshops/talks-ssh-config.pdf) |
| crypt - for the paranoid | Bob Parker      | [github](https://github.com/rlp1938/Crypt) |
