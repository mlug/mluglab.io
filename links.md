# List of links that may be useful for Linux Users.

Events - <http://www.techevents.com.au/>

## Linux Distributions Home Pages

Distrowatch = <http://distrowatch.com>

HowtoForge <http://www.howtoforge.com>

Linuxfourums = <http://www.linuxforums.org>

Linux Australia = <http://www.linux.org.au>

LinuxChix = <http://www.linuxchix.org>

## General Open Source sites

Linux related video tutorials = <http://www.osgui.com/>

GNU Operating System = <http://www.gnu.org>

Sourceforge = <http://sourceforge.net>

Freshmeat = <http://freshmeat.net>

Open Source Initiative = <http://www.opensource.org>

## Other sites of general interest

Computerbank = <http://vic.computerbank.org.au>

Melbourne Pubs = <http://www.melbournepubs.com>

## 3G wireless broadband

<http://www.sakis3g.org/>
