# LUGs of Australia

Queensland - <http://www.sci.usq.edu.au/lug/groups/LUGQld.html>

Sydney - <http://slug.org.au/>

Tas - <http://www.taslug.org.au/modules/news/>

South Australia - <http://www.linuxsa.org.au/>

Perth - <http://www.plug.org.au/>

Ballarat - <http://www.blug.asn.au/wiki/index.php5/Main_Page>
