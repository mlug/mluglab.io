# Mailing List

## Details

MLUG's mailing list is mlug-au@googlegroups.com

## How to join

Please join using the google groups website, search for mlug-au and
click join.

## How to unsubscribe

Unsubscribe information should be at the bottom of emails received from
the mailing list. If that is not working please contact one of the
Administrators via email or IRC.
