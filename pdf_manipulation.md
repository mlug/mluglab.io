# PDF Manipulation & book binding

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 25th May, 2011                                            |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Bob Parker                                                          |
| **Workshop Topic** | PDF manipulation and book binding                                   |

[pdftobook.pdf](/workshops/pdftobook.pdf)

[Bob's website](http://rlp1938.com/)
