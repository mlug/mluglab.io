# Snapshots & Openoffice macros

|                    |                                                                     |
| ------------------ | ------------------------------------------------------------------- |
| **Date:**          | Wednesday 31st August, 2011                                         |
| **Pre-meeting**    | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**        | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**     | Darren Wurf                                                         |
| **Workshop Topic** | Upgrading filesystems EXT4 -\> BTRFS and openoffice macros          |

![Snapshots slides](/workshops/snapshot-slides.odp)
