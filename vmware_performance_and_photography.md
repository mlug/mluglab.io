##### 21st Mar 2016

| Topic                            | Speaker      | Slides                                                          |
| -------------------------------- | ------------ | --------------------------------------------------------------- |
| Photography tools under Linux    | Danny Robson | [slides](/workshops/linux_photography_applications.pdf)        |
| Performance for VMs under VMware | Tyson Then   | [slides](/workshops/lug_vmware_performance_best_practices.pdf) |
