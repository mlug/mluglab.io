# VirtualBox

Date: 27th March, 2009  
Hosted: Michael Pope

![](/workshops/virtualbox.odp)

## Extra

One question raised at the workshop was, can you migrate an existing
Windows Partition to VirtualBox?

Yes, (Not the OSE version of VirtualBox). Follow the instructions
[here](http://blarts.wordpress.com/2007/12/06/how-to-run-virtualbox-using-a-physical-partition-using-ubuntu-feisty-fawn/)

It's a good idea to setup a dedicated [Hardware
Profile](http://www.dedoimedo.com/computers/hardware_profiles.html).

[Skipping
GRUB.](http://ubuntuforums.org/showpost.php?p=4848013&postcount=9)  

Full notes for Ubuntu  
http://ubuntuforums.org/showpost.php?p=6259708\&postcount=167  
<http://ubuntuforums.org/showthread.php?t=769883>

[Run the MergeIDE
from](http://www.virtualbox.org/wiki/Migrate_Windows)  
If your CPU is locked at 100% you need to follow these alterations.  
http://forums.virtualbox.org/viewtopic.php?p=33940\#33940  
  
  
  
**Warning: Raw hard disk access is for expert users only. Incorrect use
or use of an outdated configuration can lead to total loss of data on
the physical disk. Most importantly, do not attempt to boot the
partition with the currently running host operating system in a guest.
This will lead to severe data corruption.**
