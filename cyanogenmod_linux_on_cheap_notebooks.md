##### 18th Apr, 2016

| Topic                                              | Speaker | Slides                                         |
| -------------------------------------------------- | ------- | ---------------------------------------------- |
| Cyanogenmod & Sailfish                             | Cameron | [slides](/workshops/20160418-cyanogenmod.pdf) |
| Installing linux on a very cheap and nasty laptop. | Cameron | [slides](/workshops/20160418-laptop.pdf)      |
