\*\* 29th May, 2017 \*\*

| Topic                                              | Speaker      | Slides                                        |
| -------------------------------------------------- | ------------ | --------------------------------------------- |
| cross compiling and distcc                         | Rick Miles   | ![slides](/workshops/crossc-dstcc.odp)        |
| Install extra applications natively on Chromebooks | Michael Pope | [slides](/workshops/20170529-chromebrew.pdf) |
