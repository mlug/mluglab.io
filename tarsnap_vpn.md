**Topics for \*MONDAY\* 27th April**

| Topic                     | Presenter    | Slides                                                |
| ------------------------- | ------------ | ----------------------------------------------------- |
| tarsnap (lightening talk) | Darren Wurf  | [tarsnap website](http://www.tarsnap.com/index.html)  |
| VPN comparison            | Michael Pope | [VPN slides](/workshops/20150427-vpn_comparison.pdf) |
