# SSH Tricks and Screen

**Wednesday 27th February, 2013 @ 7pm (pre-meeting downstairs at
6pm)**  
**Address** Muttis upstairs (118 Elgin st, Carlton VIC)

## SSH - Tips n' Tricks by David Schoen

[SSH Tips and Tricks](/workshops/ssh_tips_n_tricks.pdf)

## The 'screen' tool by Malcolm Herbert

[Screen Slides](/workshops/screen.pdf)

The git repository which includes the above slide pack, the config
examples and helper scripts may be had by doing the following:

    git clone http://deimos.ergonaut.org/repository/talks-screen

If there were any suggestions around the content of the talk, please let
me know.
