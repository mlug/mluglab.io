# MythTV install & customise

Date: 28th Mar 2008  
Hosted: Michael Pope

Also refer to notes I've made on Mythtv at:
<http://linux-mlug.blogspot.com/>

## Hardware I recommend

Dual tuners  
\* Haupauge Nova T-500 (OEM) - $200 (digiworld)  
Single tuners  
\* Haupauge Nova T (WinTV) - Approx. $60 (second hand)

  - Twinhan DTV PCI - $49 (msy)
    <http://www.mythtv.org/wiki/index.php/Twinhan_MiniTer_DVT_PCI>

  
Remote Control  
\* Microsoft MCE Remote (Phillips model which you can tell from the 4
coloured buttons at the bottom of the remote).  
USB Tuners  
\* Avertv Volar -
<http://www.mythtv.org/wiki/index.php/AVerTV_DVB-T_Volar>

  - Asus U3000 (haven't tested this with mythtv but it works on Linux &
    the eeepc)

Note: USB Tuners tend to have weaker signal strength than single tuner
PCI cards, so if you are in a weak area get a single tuner card.

## Install Mythbuntu 710

1.  Insert Mythbuntu 710 live cd
2.  Double click on 'Install Mythbuntu' (Warning this will format your
    HD and install mythbuntu, not straight away without warning though).
3.  Step 1: First screen is lang selection, Hit 'Forward' to accept the
    default 'English'
4.  Step 2: Next click on Melbourne, Australia on the map and hit
    'Forward'
5.  Step 3: Hit 'Forward' to accept the default Keyboard selection
6.  \*\* Step 4: Is important as you select your partition for
    installation, I've chosen Entire HD \*\*
7.  Step 5: Enter your name or computer name and password into the next
    step. 
8.  Step 6: Select 'standard installation'
9.  Step 12: Setup your remote control, I use a Mircrosoft MCE Remote
    (Phillips chipset) as it's easy to setup and lights up in the dark
10. Step 13: Click 'Modify Video Driver & Settings' and check for a
    driver in the drop down box for your card. I use Nvidia chipset
    based cards as they are the easiest to get working with Mythtv and
    with TV Out. Click 'Forward'
11. Step 14: Check your settings and click 'Install'
12. Step 15: Click 'Finish' & 'Restart Now'

## Configure TV card

Make sure you have a well tested antenna and your TV card or USB TV
tuner is connected when you boot.

When you boot for the first time you will notice that the system boots
directly into the mythtv frontend. You still need to configure your TV
card so you have to exit this by hitting ESC, then selecting Yes.

To configure your TV card:

1.  Go to the menu item 'Applications -\> System -\> Mythtv backend
    setup'
2.  Click 'OK' to closing down the mythtv backend
3.  Enter your password which you entered during setup.

1\. General Settings Area.

1.  Hit Enter on 'General'
2.  Hit enter twice and you should be at the 'Global Backend Setup' page
3.  Select TV format as 'PAL' and Channel Frequency table as
    'australia', 
4.  Keep Hitting 'next' till you are back at the main menu.

2\. Capture Card

1.  Hit Enter on 'Capture Cards'
2.  Go to 'New capture card'
3.  Select Card type as 'DVB DTV Capture Card (v 3.x) if you are using a
    digital TV card. Your card should be shown beside the lable
    'Frontend ID'. If it's not try a different card type.

3\. Video Sources

1.  Hit Enter on 'Video Sources'
2.  Go to 'New video source'
3.  Give it a name, I usually use 'tv' as my name.
4.  Set listings grabber to 'No Grabber' for now

4\. Input Connections

1.  Hit Enter on 'Input Connections'
2.  Select your Tuner card from the list
3.  Change Video Source to the video source you define before.
4.  Hit Enter on 'Scan for channels'
5.  Go with the defaults and hit 'next'

5\. Channel Editor

1.  Hit Enter on 'Channel Editor' 
2.  Check in here that you have all the channels, otherwise you might
    have to look at upgrading your cabling, antenna or boosters to
    improve signal strength.

To test your tuner open up a Terminal by going through the menu system
'Applications -\> Accessories -\> Terminal' and type: 'sudo
mythbackend'.

Now start the frontend by 'Applications -\> Multimedia -\> Mythtv
Frontend'

### Using a computer screen as your TV

If you use a computer screen instead of a TV screen, like me, then you
will need to turn off interlacing:

In the mythfrontend

1.  Go to 'Utilities/Setup -\> Setup -\> TV Settings -\> Playback'
2.  Enable 'Deinterlace playback'
3.  Click Next until the end.

### USB & Some Dual Tuners

If you use a USB tuner or a Dual Tuner card which has USB tuners on the
card then you most likely require the dvb-usb-dib0700-01.fw firmware
file which cannot be included because it's not open source and the
licenses. So you will have to search for this, but it's best to download
**dvb-usb-dib0700-03-pre1.fw** .  
Download from:  
<http://www.wi-bw.tfh-wildau.de/~pboettch/home/linux-dvb-firmware/dvb-usb-dib0700-03-pre1.fw>
Via command line  
$ wget
<http://www.wi-bw.tfh-wildau.de/~pboettch/home/linux-dvb-firmware/dvb-usb-dib0700-03-pre1.fw>

Once you have downloaded that file, as root do this command:

    # cp dvb-usb-dib0700-03-pre1.fw /lib/firmware/dvb-usb-dib0700-01.fw

Now if you use a USB tuner you can simply unplug and replug the tuner,
otherwise if you are using a PCI card which just happens to have a USB
chip on board then you will need to reboot.

For the Haugpauge Nova T-500 refer:
<http://www.mythtv.org/wiki/index.php/Hauppauge_WinTV_Nova-T_500_PCI>

## Install EPG (Shepherd)

<http://svn.whuffy.com/>
