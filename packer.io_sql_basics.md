# Packer.io & SQL Basics

\*\* March 2014 \*\*

|            |         |                                                 |
| ---------- | ------- | ----------------------------------------------- |
| packer.io  | Malcolm | <https://packer.io/>                            |
| SQL Basics | Darren  | <http://dwurf.github.io/mlug-sql-tute-beginner> |
