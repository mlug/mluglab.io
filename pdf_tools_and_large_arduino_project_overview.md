# Pdf tools and large Arduino project overview

|                   |                                                                           |
| ----------------- | ------------------------------------------------------------------------- |
| **Date:**         | 30th May, 2012                                                            |
| **Pre-meeting**   | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                      |
| **Meeting**       | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\]       |
| **Hosted By:**    | Bob Parker & George Patterson                                             |
| **Meeting Topic** | C programming, pdf tools & rsync helpers. Arduino large project overview. |

## Arduino large project

![](/workshops/20120530-guerrillatalk.odp)

## pdf page break tools written in C
