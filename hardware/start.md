# Hardware

Tips and tricks from our members on hardware they have got working.

[Michael's tested hardware
spreadsheet](https://docs.google.com/spreadsheet/ccc?key=0AuZCnWTteFtRdExYekRGLXRUSEl3emNubUJZQUFjc1E)

[Thin
Clients](https://docs.google.com/spreadsheet/ccc?key=0AuZCnWTteFtRdDlEUmxwQVVRWmZIT1lneWs2YWtNVmc&usp=sharing)

[Notebooks](https://docs.google.com/spreadsheets/d/143tzxb-iehb7krznhrq3l84cmmxwgqopigrhhujetig/edit?usp=sharing)

## Linux Hardware compatibility lists

Freedom Software Foundation Hardware compatibility list
<https://h-node.org/>

<http://www.linux-drivers.org/>

<http://www.openprinting.org/printers>
