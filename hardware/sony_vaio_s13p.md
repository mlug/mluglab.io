# Sony Vaio S13P

\*UPDATE: (Apr 2015) I recently put Linux Mint 17 on this and it works
perfectly and has fixed a lot of problems such as the mouse works great
now.\*

Here are my notes on what I have working.

Yet to get working is

  - Work out why WIFI is so flaky. Doesn't work with all routers and
    drops signal.
  - Get Nvidia card working for certain applications using something
    like: <http://bumblebee-project.org/>
  - Turbo CPU mode for longer that a split second.
  - Control fan levels even more.
  - Get finger reader working

## Crashing/Fan problem

When I installed a copy of Linux Mint 13 on this computer it crashed a
lot sometimes three times a night. The fan was also going at full speed
most of time even when just sitting there. It makes a lot of noise and
it's hard to work with.

I found the problem was the that the Nvidia card was actually on even
though I had switched the switch to 'Stamina'.

To fix both of these problem I turned the Nvidia card off. This requires
you to upgrade the kernel and use the bbswitch.

I started looking at Fan controls for the Sony Vaio S13P but the closest
I found didn't work: <http://vaio-utils.org/fan/>. The problem here I
think is that this is for older models than mine and most of the ACPI
devices are not showing up in the kernel.

### Upgrade to the latest kernel

[How To Upgrade your kernel,
painlessly](http://forums.linuxmint.com/viewtopic.php?f=42&t=40185&start=0)

Upgraded from 3.2 to 3.4.0-030400-generic 64bit.

This combined with the bbswitch has stopped the freezing problem.

### bbswitch

The bbswitch is used to disable the Hybrid Graphics card.

[Comparison of PM
methods](https://github.com/Bumblebee-Project/Bumblebee/wiki/Comparison-of-PM-methods)
[bbswitch git repository](https://github.com/bumblebee-project/bbswitch)

Compile the bbswitch

``` 
   git clone https://github.com/Bumblebee-Project/bbswitch.git
   cd bbswitch
   make -j4
   sudo make load
   sudo /sbin/insmod bbswitch.ko
   sudo make load
```

Get status

    cat /proc/acpi/bbswitch

Manually switch off

    sudo tee /proc/acpi/bbswitch <<<OFF

Check status, first the proc, then the last line of dmesg.

    cat /proc/acpi/bbswitch
    dmesg | tail -1

Dmesg output should look like:

    : [ 1727.770947] bbswitch: version 0.4.2
    : [ 1727.770953] bbswitch: Found integrated VGA device 0000:00:02.0: \_SB_.PCI0.GFX0
    : [ 1727.770960] bbswitch: Found discrete VGA device 0000:01:00.0: \_SB_.PCI0.PEG0.PEGP
    : [ 1727.771008] bbswitch: detected an Optimus _DSM function
    : [ 1727.771014] bbswitch: Succesfully loaded. Discrete card 0000:01:00.0 is on
    : [ 1766.857393] bbswitch: Unloaded. Discrete card 0000:01:00.0 is on
    : [ 1766.858123] bbswitch: version 0.4.2
    : [ 1766.858130] bbswitch: Found integrated VGA device 0000:00:02.0: \_SB_.PCI0.GFX0
    : [ 1766.858136] bbswitch: Found discrete VGA device 0000:01:00.0: \_SB_.PCI0.PEG0.PEGP
    : [ 1766.858194] bbswitch: detected an Optimus _DSM function
    : [ 1766.858199] bbswitch: Succesfully loaded. Discrete card 0000:01:00.0 is on
    : [ 1801.720954] bbswitch: disabling discrete graphics
    : [ 1801.721091] bbswitch: Result of Optimus _DSM call: 01000059
    : [ 1801.736774] pci 0000:01:00.0: power state changed by ACPI to D3

Install as dkms so that it builds on new kernels

    sudo dkms remove bbswitch/OLDVERSION --all
    sudo make -f Makefile.dkms

Add the following to /etc/modules

    bbswitch load_state=0

Blacklist the open source nvidia drivers from starting

Add the following to /etc/modprobe.d/blacklist.conf

    blacklist vga16fb
    blacklist nouveau
    blacklist rivafb
    blacklist nvidiafb
    blacklist rivatv

Update the boot process.

    sudo update-initramfs -u

Now reboot and make sure your card is off

### Optimus Nvidia setup

1\. Install bumblebee and nvidia tools

``` 
 : sudo apt-get install bumblebee primus bumblebee-nvidia nvidia-detect nvidia-driver
```

2\. Add the following to the kernel boot parameters

``` 
 : rcutree.rcu_idle_gp_delay=1
  
 Make it persistent after reboot
 edit /etc/default/grub
 Adjust the following to look like this;
 : GRUB_CMDLINE_LINUX_DEFAULT="rcutree.rcu_idle_gp_delay=1 quiet"
```

``` 
 re-generate grub.cfg
 : sudo grub-mkconfig -o /boot/grub/grub.cfg
```

3\. Install glxspheres (use instead of glxgears) - For tests

``` 
 1. Download VirtualGL (.deb) from:http://sourceforge.net/projects/virtualgl/files/2.4/ 
    : wget http://downloads.sourceforge.net/project/virtualgl/2.4/virtualgl_2.4_amd64.deb
 2. Navigate to the folder containing the deb package and install it with:
    : sudo dpkg -i VirtualGL_*.deb
 3. Run glxspheres:
    : /opt/VirtualGL/bin/glxspheres64
 4. Link
    : sudo ln -s /opt/VirtualGL/bin/glxspheres64 /usr/local/bin/glxspheres
```

4\. Results

``` 
 | Desc      | command                            | FPS |
 |-----------+------------------------------------+-----|
 | original  | vblank_mode=0 glxspheres           | 120 |
 | optirun   | vblank_mode=0 optirun glxspheres   | 145 |
 | primusrun | vblank_mode=0 primusrun glxspheres | 190 |
```

## Touchpad

The Sony Vaio touchpad is what they call a 'ClickPad'. There are no
buttons and it's one big touch surface. Linux Mint 13 isn't setup to
handle this out of the box so you have to do the following to fix the
problem.

Create directory

``` 
  sudo mkdir /etc/X11/xorg.conf.d
```

Create file /etc/X11/xorg.conf.d/synaptics.conf

``` 
  sudo gedit /etc/X11/xorg.conf.d/synaptics.conf
```

Add the following to that file

``` 
 Section "InputClass"
        Identifier "touchpad catchall"
        Driver "synaptics"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Option "LeftEdge"               "2900"
        Option "RightEdge"              "4000"
        Option "TopEdge"                "1620"
        Option "BottomEdge"             "3800"
        Option "TapButton1"             "1"
        Option "TapButton2"             "3"
        Option "TabButton3"             "2"
        Option "FastTaps"               "1"
        Option "CornerCoasting"         "1"
        Option "CoastingSpeed"          "0.30"
        Option "PalmDetect"             "true"
        Option "VertTwoFingerScroll"    "true"
        Option "HorizTwoFingerScroll"   "true"
        Option "SHMConfig"              "on"
        Option "SendCoreEvents"
        Option "MaxDoubleTapTime"       "50"
        Option "LBCornerButton"         "1"
        Option "RBCornerButton"         "3"
        Option "FingerLow"              "7"
        Option "FingerHigh"             "8"
        Option "MaxTapTime"             "180"
        Option "MaxTapMove"             "110"
        Option "VertScrollDelta"        "20"
        Option "HorizScrollDelta"       "20"
        Option "MinSpeed"               "0.60"
        Option "MaxSpeed"               "1.10"
        Option "AccelFactor"            "0.030"
        Option "EdgeMotionMinpeed"      "200"
        Option "EdgeMotionMaxSpeed"     "200"
        Option "SoftButtonAreas"        "4000 0 3800 0 2000 3999 3800 0"
 EndSection
```

Save & reboot your computer.

Now you should have a left, middle & right button at the bottom of your
touchpad and you should be able to drag and drop.
