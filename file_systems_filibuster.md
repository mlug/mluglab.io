## File Systems Filibuster

|                           |                                                                     |
| ------------------------- | ------------------------------------------------------------------- |
| **Date:**                 | Wednesday 23rd February, 2011                                       |
| **Pre-meeting**           | 6pm @ Muttis downstairs (118 Elgin st, Carlton VIC).                |
| **Meeting**               | 7pm @ Muttis upstairs (118 Elgin st, Carlton VIC). \[Take Tram 96\] |
| **Hosted By:**            | Richard Cottrill                                                    |
| **Workshop Topic**        | Richard's file systems filibuster                                   |
| **Workshop Requirements** |                                                                     |
