**30th April 2018**

| Talk                         | Speaker      | Slides                                     |
| ---------------------------- | ------------ | ------------------------------------------ |
| Static Blogging with Pelican | Danny Robson | [slides](/workshops/20180430-pelican.pdf) |
| xdotool                      | Michael Pope | [slides](/workshops/20180430-xdotool.pdf) |
