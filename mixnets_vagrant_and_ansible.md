# Mixnets & Vagrant and Ansible

**Meeting Topics for 28th May**

| Talk                                                  | Presenter     | Slides/Info                                                                                                                              |
| ----------------------------------------------------- | ------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| Surveillance and internet security using mixnets      | Tim Rice      | [git repo](https://github.com/cryptarch/mix)                                                                                             |
| Web development environments with Vagrant and Ansible | Damien Butler | [Slides](http://www.doublehops.com/presentations/vagrant-ansible/#/) [git repo](https://github.com/doublehops/debian-webdev-environment) |
| Describe your rig setup                               | Open Forum    |                                                                                                                                          |
