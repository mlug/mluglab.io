Here are some extra examples which Bob is working on at the moment. If
anyone wants to explore more advanced features than the jumble tutorial
you can play around with these files.

Step 1. Download the ![Code Bundle](/tutorials/c/when.tgz)

program 'when' produces stuff like this:

ICE Linux Frankston Regular Meetings  
19:30 Fri 9-May-2008  
19:30 Thu 15-May-2008  
19:30 Thu 22-May-2008  
19:30 Fri 13-Jun-2008  
19:30 Thu 19-Jun-2008  
19:30 Thu 26-Jun-2008  
19:30 Fri 11-Jul-2008  
19:30 Thu 17-Jul-2008  
19:30 Thu 24-Jul-2008  
or this:  
MLUG Meetings Next 12 Months  
7:30 PM Fri 30-May-2008  
7:30 PM Fri 27-Jun-2008  
7:30 PM Fri 25-Jul-2008  
7:30 PM Fri 29-Aug-2008  
7:30 PM Fri 26-Sep-2008  
7:30 PM Fri 31-Oct-2008  
7:30 PM Fri 28-Nov-2008  
7:30 PM Fri 30-Jan-2009  
7:30 PM Fri 27-Feb-2009  
7:30 PM Fri 27-Mar-2009  
7:30 PM Fri 24-Apr-2009  
There are 3 programs in the bundle when.c, when\_nl.c, and every.c along
with some source library.

Only when\_nl.c and every.c are useful, the value of when.c is that it
illustrates the use of linked lists whilst doing nothing any different
to when\_nl.c (when-nolists).

Compile as follows (within When dir):  
gcc -Wall when\_nl.c -o when  
gcc -Wall every.c -o every  
If you want to play with the list version of when compile this way:  
gcc -Wall when.c xlist.c -o when  
xlist.? has symlinks in When/ to Boilerplate/xlist.? so it should all
just compile and link.

The Boilerplate stuff is required for most of the projects I'm cooking
at the moment.

BTW Once compiled just ./when -h | less. All of my stuff has a help file
like that if it has any options (Jumble and friends do not).
