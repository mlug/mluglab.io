# Q CuBox Proxmox

**28th November, 2012 @ 7pm (pre-meeting at 6pm)**

**Address** Muttis downstairs (118 Elgin st, Carlton VIC)

**Meeting Topics**

1.  Overview of 'Q' editor by Duncan Roe
2.  Demo of CuBox by David Burgess
3.  Proxmox VE and Turnkey Linux by Renato Luminati 

## Q notes

Up on github at <https://github.com/duncan-roe/q>

Called Q because of its versatile q command

Unremarkable editor greatly enhanced by its macro facility

Command-line based, command lines can themselves be edited

Most control characters do something

Tries to resolve the "tabs or spaces" dilemna

Case-independent

Has concept of "tokens"

(Start q \~/q/TODO, describe macro loading & symlink res'n) (Leave TODO
up for adding stuff) (insert request for macro catalogue)

Uses mmap / deferred file read

Binary mode

Might like to set xterm option "Delete is DEL" from menu or put
"xterm\*vt100.deleteIsDEL: true" in .Xresources

Massages commands for minimal typing

Macros introduced by Control-N (^N).

Macros in range 0 to 03777 except pseudo-macros '@' to '\~' (including
alphabet which is case-insensitive)

"Active pseudo-macros" in range 04000 - 07777 (only a few implemented so
far)

(Run through some macros I find useful: ^N^K ^N^N ^N^I/^N^H ^N^E/^N^L
^N^W\[pqr\] ^N^V/^N^Q (& fm+h) ^N^W^G (needs runindent) ^N^W^F ^N^W^X
...)

Github repo: <https://github.com/duncan-roe/command_line_tools.git>
